<?php



//SITE_ROOT
$path = $_SERVER['DOCUMENT_ROOT'] . '/FarmixShop/backend';
define('SITE_ROOT', $path);


//SITE_PATH
define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . '/FarmixShop/');


//model
define('MODEL_PATH', SITE_ROOT . '/model/');

//modules
define('MODULES_PATH', SITE_ROOT . '/modules/');

//resources
define('RESOURCES', SITE_ROOT . '/resources/');

//media
define('MEDIA_ROOT', SITE_ROOT . '/media/');
define('MEDIA_PATH', SITE_PATH . 'backend/media/');

//utils
define('UTILS', SITE_ROOT . '/utils/');

//libs
define('LIBS', SITE_ROOT . '/libs/');

//CSS
define('CSS_PATH', SITE_PATH . 'view/css/');

//JS
define('JS_PATH', SITE_PATH . 'view/js/');

//IMG
define('IMG_PATH', SITE_PATH . 'view/images/');

//view
define('VIEW_PATH_INC', SITE_ROOT . 'view/inc/');

define('PRODUCTION', true);

//model ofertas
define('MODEL_OFERTAS', SITE_ROOT . '/modules/ofertas/model/model/');

//model home
define('MODEL_HOME', SITE_ROOT . '/modules/home/model/model/');

//MODEL CONTACT
define('CONTACT_JS_PATH', SITE_PATH . 'modules/contact/view/js/');

//UTILS LOGIN
define('UTILS_LOGIN', SITE_ROOT . '/modules/login/utils/');

//UTILS CONTACT
define('UTILS_CONTACT', SITE_ROOT . '/modules/contact/utils/');
//amigables
define('MODEL_LOGIN', SITE_ROOT . '/modules/login/model/model/');
define('URL_AMIGABLES', TRUE);
