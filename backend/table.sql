CREATE TABLE usuarios (
    `id_usuario` varchar(100) NOT NULL,
    `nikname` varchar(255)CHARACTER SET latin1 NOT NULL,
    `email` varchar(255) CHARACTER SET latin1 NOT NULL,
    `password` varchar(200) CHARACTER SET latin1 NOT NULL,
    `nombre` varchar(200) CHARACTER SET latin1 NOT NULL,
    `pais` varchar(200) CHARACTER SET latin1 NOT NULL,
    `provincia` varchar(200) CHARACTER SET latin1 NOT NULL,
    `poblacion` varchar(200) CHARACTER SET latin1 NOT NULL,
    `avatar` varchar(200) CHARACTER SET latin1 NOT NULL,
    `tipo` varchar(200) CHARACTER SET latin1 NOT NULL,
    `activado` varchar(200) CHARACTER SET latin1 NOT NULL,
    `token` varchar(200) CHARACTER SET latin1 NOT NULL,
    `emp_name` varchar(200) CHARACTER SET latin1 NOT NULL,
    `latitud` varchar(200) CHARACTER SET latin1 NOT NULL,
    `longitud` varchar(200) CHARACTER SET latin1 NOT NULL,
    PRIMARY KEY (`id_usuario`)
);
CREATE TABLE productos (
    `id_producto` varchar(255) CHARACTER SET latin1 NOT NULL,
   	`id_usuario` varchar(255) CHARACTER SET latin1 NOT NULL,
    `producto` varchar(255) CHARACTER SET latin1 NOT NULL,
    `tipo` varchar(255) CHARACTER SET latin1 NOT NULL,
    `cantidad` varchar(255) CHARACTER SET latin1 NOT NULL,
    `precio` varchar(255) CHARACTER SET latin1 NOT NULL,
    `temporada` varchar(255) CHARACTER SET latin1 NOT NULL,
    PRIMARY KEY (`id_producto`)
)
ENGINE=InnoDB;

ALTER TABLE productos 
ADD CONSTRAINT FK_idproducto
FOREIGN KEY (id_usuario) REFERENCES usuarios(id_usuario) 
ON UPDATE CASCADE
ON DELETE CASCADE;