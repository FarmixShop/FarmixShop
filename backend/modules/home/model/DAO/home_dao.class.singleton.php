<?php
class home_dao {
    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function search_home_DAO($db, $arrArgument) {
   $sql = "select distinct c.country, c.capital from countries c order by 1";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

  
}
