<?php
function sendtoken($arrArgument, $type) {
    $mail = array(
        'type' => $type,
        'token' => $arrArgument['token'],
        'inputEmail' => $arrArgument['email']
    );
    set_error_handler('ErrorHandler');
    try {
       $res =  send_mailgun($mail);
        return $res;
    } catch (Exception $e) {
        return send_mailgun($mail);
    }
    restore_error_handler();
}
