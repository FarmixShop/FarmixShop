<?php
class controller_ofertas {

    function __construct() {
    }

    /**
     *  We take all offers from BD and we return them
     *
     * @return mixed[] Returns an array['success']=boolean and if it is true we return the array array['ofertas']=array with all offers.
     */
    function oferload() {
        set_error_handler('ErrorHandler');
        
                $arrValue = loadModel(MODEL_OFERTAS, "ofertas_model", "count_ofers");
                restore_error_handler();
        
                if ($arrValue) {
                    $arrArguments['ofertas'] = $arrValue;
                    $arrArguments['success'] = true;
                    echo json_encode($arrArguments);
                } else {
                    $arrArguments['ofertas'] = $arrValue;
                    $arrArguments['success'] = false;
                    $arrArguments['error'] = 503;
                    echo json_encode($arrArguments);
                }
    }
    function maploader() {
        set_error_handler('ErrorHandler');

        $arrValue = loadModel(MODEL_OFERTAS, "ofertas_model", "count_user");
        restore_error_handler();

        if ($arrValue) {
            $arrArguments['ofertas'] = $arrValue;
            $arrArguments['success'] = true;
            echo json_encode($arrArguments);
        } else {
            $arrArguments['ofertas'] = $arrValue;
            $arrArguments['success'] = false;
            $arrArguments['error'] = 503;
            echo json_encode($arrArguments);
        }
    }
    function getUser() {
        set_error_handler('ErrorHandler');
        $arrArguments = array(
            'like' => $_GET['param'],
        );
        try {
            $arrValue = loadModel(MODEL_OFERTAS, "ofertas_model", "select_users",$arrArguments);
        } catch (Exception $e) {
            $arrValue = false;
        }
        restore_error_handler();

        if ($arrValue) {
            $arrArguments['ofertas'] = $arrValue[0];
            $arrArguments['success'] = true;
            echo json_encode($arrArguments);
        } else {

            $arrArguments['success'] = false;
            $arrArguments['error'] = 503;
            echo json_encode($arrArguments);
        }

    }
    function getOffer() {
        $arrArguments = array(
            'like' => $_GET['param'],
        );
        set_error_handler('ErrorHandler');
        try {
            $arrValue = loadModel(MODEL_OFERTAS, "ofertas_model", "select", $arrArguments);
        } catch (Exception $e) {
            $arrValue = false;
        }
        restore_error_handler();

        if ($arrValue) {
            $arrArguments['ofertas'] = $arrValue;
            $arrArguments['success'] = true;
            echo json_encode($arrArguments);
        } else {

            $arrArguments['success'] = false;
            $arrArguments['error'] = 503;
            echo json_encode($arrArguments);
        }
    }

    function join() {
        set_error_handler('ErrorHandler');
        try {
            $arrValue = loadModel(MODEL_OFERTAS, "ofertas_model", "update", array('column' => array('id_usuario'), 'like' => array($_GET['param']), 'field' => array('asistentes'), 'new' => array($_GET['param2'])));
        } catch (Exception $e) {
            $arrValue = false;
        }
        restore_error_handler();

        if ($arrValue) {
            $arrArguments['datos'] = $_GET['param2'];
            $arrArguments['success'] = true;
            echo json_encode($arrArguments);
        } else {
            $arrArguments['success'] = false;
            $arrArguments['error'] = 503;
            echo json_encode($arrArguments);
        }
    }
}
