<?php
  function send_mailgun($arr){
  // $users = get_current_user();
   $config = array();
   $config['api_key'] = "key-51602e9178a06ed9a9f030378245367e"; //API Key
   $config['api_url'] = "https://api.mailgun.net/v2/sandboxde981e972f7f4f01ad51cfcf2cd19cad.mailgun.org/messages"; //API Base URL

   switch ($arr['type']) {
    case 'alta':
        $subject = 'Tu Alta en FarmixShop ';
        $ruta = "<a href='http://".$_SERVER['HTTP_HOST']."/FarmixShop/#/user/activar/" . $arr['token'] . "'>aqu&iacute;</a>";
        $body = 'Gracias por unirte a nuestra aplicaci&oacute;n. Para finalizar el registro, pulsa ' . $ruta;
        break;

    case 'modificacion':
        $subject = 'Tu Nuevo Password en FarmixShop ';
        $ruta = "<a href='http://".$_SERVER['HTTP_HOST']."/FarmixShop/#/login/cambiarpass/" . $arr['token'] . "'>aqu&iacute;</a>";
        $body = 'Para recordar tu password pulsa ' . $ruta;
        break;
    case 'contact':
        $subject = 'Tu Petici&oacute;n a FarmixShop ha sido enviada<br>';
        $ruta = '<a href="http://'.$_SERVER['HTTP_HOST'].'//FarmixShop/#/"' . '>aqu&iacute;</a>';
        $body = 'Para visitar nuestra web, pulsa ' . $ruta;
        break;

    case 'admin':
        $subject = $arr['inputSubject'];
        $body = 'inputName: ' . $arr['inputName'] . '<br>' .
                'inputEmail: ' . $arr['inputEmail'] . '<br>' .
                'inputSubject: ' . $arr['inputSubject'] . '<br>' .
                'inputMessage: ' . $arr['inputMessage'];
        break;
}

   $message = array();
   $message['from'] = $arr['inputEmail'];
   $message['to'] = $arr['inputEmail'];
   $message['h:Reply-To'] = $arr['inputEmail'];
   $message['subject'] = 'Support '.$arr['inputEmail'];
   $message['html'] = '' . $arr['inputEmail'] . ' have a '.$subject.'support issue opened <br> Type'.$body.'';

   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $config['api_url']);
   curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
   curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
   curl_setopt($ch, CURLOPT_POST, true);
   curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
   $result = curl_exec($ch);
   curl_close($ch);
   return $result;
 }
?>
