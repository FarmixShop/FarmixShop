// Creación del módulo
var farmixShop = angular.module('farmixShop', ['ngRoute','ngCookies', 'ui.bootstrap', 'ngAnimate','facebook','ngMap','ngSanitize']).run(function($rootScope) {
	$rootScope.$on('mapInitialized', function(evt,map) {
	  $rootScope.map = map;
	  $rootScope.$apply();
	});
  });

// Configuración de las rutas
farmixShop.config(function($routeProvider) {

	$routeProvider
		.when('/', {
			templateUrl	: 'frontend/modules/home/view/home.html',
			controller 	: 'mainController'
		})

		.when('/contacto', {
			templateUrl : 'frontend/modules/contact/view/contact.html',
			controller 	: 'contactController'
		})

		.when('/cosecha', {
			templateUrl : 'frontend/modules/cosecha/view/cosecha.html',
			controller  : 'cosechaController',
			resolve: {
					ofertas: function (services) {
						console.log(services.get('ofertas', 'maploader'));
							return services.get('ofertas', 'maploader');
							
					},
					productos: function (services){
						return services.get('ofertas', 'oferload');
					}
			}
		})
		.when("/cosecha/:id", {
				templateUrl: "frontend/modules/cosecha/view/detailsCosecha.html",
				controller: "detailsCtrl",
				resolve: {
						data: function (services, $route) {
								return services.get('ofertas', 'getOffer', $route.current.params.id);
						},
						user: function (services, $route) {
							return services.get('ofertas', 'getUser', $route.current.params.id);
					}
				}
		})
		//Signup
		.when("/user/alta/", {
				templateUrl: "frontend/modules/login/view/signup.view.html",
				controller: "signupCtrl"
		})
		//Activar Usuario
		.when("/user/activar/:token", {
				templateUrl: "frontend/modules/home/view/home.html",
				controller: "verifyCtrl"
		})

		//Perfil
		.when("/user/profile/", {
				templateUrl: "frontend/modules/login/view/profile.view.html",
				controller: "profileCtrl",
				resolve: {
						user: function (services, cookiesService) {
								var user = cookiesService.GetCredentials();
								if (user) {
									console.log(services.get('login', 'profile_filler', user.usuario));
										return services.get('login', 'profile_filler', user.usuario);
								}
								return false;
						}
				}
		})
		.when("/login/recuperar", {
			templateUrl: "frontend/modules/login/view/restore.view.html",
			controller: "restoreCtrl"
		})
		//ChangePass
		.when("/login/cambiarpass/:token", {
			templateUrl: "frontend/modules/login/view/changepass.view.html",
			controller: "changepassCtrl"
		})

		.otherwise({
			redirectTo: '/'
		});
});
farmixShop.config([
    'FacebookProvider',
    function (FacebookProvider) {
        var myAppId = '372766359835211';
        FacebookProvider.init(myAppId);
    }
]);
