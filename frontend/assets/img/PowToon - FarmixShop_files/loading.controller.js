angular.module('publishModalApp')
    .controller('LoadingController', LoadingController);

LoadingController.$inject = ['$scope'];
function LoadingController($scope) {
    angular.extend($scope.$parent.modal, {
        nextEnabled: false,
        backEnabled: false,
        hideSteps: true
    });
};