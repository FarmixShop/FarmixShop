(function () {
    'use strict';

    angular.module('publishModalApp')
        .factory('Publish', Publish);

    function Publish() {
        var publishTo;
        var powtoon;
        var mode;
        var category;
        var options = {};
        var userData = {};

        function init(powtoon, mode, category, publishTo, name, isRepublished, description, tags, powtoonCategory, aspectRatio) {
            this.powtoon = powtoon;
            this.publishTo = publishTo;
            this.mode = mode;
            this.category = category;

            this.options = {
                watermark: true,
                noWatermark: true,
                unlist: {id: 0, text: "Public"},
                uploadTo: '',
                quality: '480',
                youtubeAccount: '',
                facebookAccount: '',
                wistiaAccount: '',
                vimeoAccount: '',
                hubspotAccount: '',
                slideshareAccount: {
                    username: '',
                    password: ''
                },
                square: aspectRatio === 'square',
                form: {
                    title: ''
                }
            };

            this.userData = {
                isPremium: false,
                isAgency: false,
                youtube_accounts: [],
                facebook_accounts: [],
                wistia_accounts: [],
                vimeo_accounts: [],
                hubspot_accounts: [],
                wistia_url: "",
                vimeo_url: '',
                hubspot_url: '',
                facebook_url: '',
                ut: window.ut_data || "",
                plan: {},
                thumb: "",
                republished: isRepublished,
                form: {
                    tags: tags.length ? tags.join(',') : '',
                    title: name || '',
                    category: powtoonCategory ?
                    {
                        id: powtoonCategory.id,
                        text: powtoonCategory.name
                    } :
                    {
                        id: 0
                    },
                    desc: (description != 'None') ? description : '',
                    privacy: {id: 0, text: 'Public'}
                }
            };
        }


        function publish() {
            var that = this;
            var tagsArray = this.userData.form.tags.length ? this.userData.form.tags.split('\n').join(",").split(',') : [];

            var indexed = 'on';

            if (this.options.unlist.id === 2) {
                indexed = '';
            }

            var exportParams = {
                mode: this.mode || "",
                youtube: ( this.options.uploadTo == "youtube") ? "on" : "",
                facebookupload: ( this.options.uploadTo == "facebookupload") ? "on" : "",
                wistia: ( this.options.uploadTo == "wistia") ? "on" : "",
                vimeo: ( this.options.uploadTo == "vimeo") ? "on" : "",
                hubspot: ( this.options.uploadTo == "hubspot") ? "on" : "",
                slideshare: ( this.options.uploadTo == "slideshare") ? "on" : "",
                ppt: ( this.options.uploadTo == "ppt") ? "on" : "",
                pdf: ( this.options.uploadTo == "pdf") ? "on" : "",
                offline_player: ( this.options.uploadTo == "offline_player") ? "on" : "",
                download: ( this.options.uploadTo == "mp4") ? "on" : "",
                hashid:  this.powtoon,
                indexed: indexed,
                quality:  this.options.quality,
                lic:  this.userData.plan.lic_id,
                facebook_id: ( this.options.uploadTo === "facebookupload") ? this.options.facebookAccount : 0,
                youtube_id: ( this.options.uploadTo === "youtube") ? this.options.youtubeAccount : 0,
                wistia_id: ( this.options.uploadTo === "wistia") ? this.options.wistiaAccount : 0,
                vimeo_id: ( this.options.uploadTo === "vimeo") ? this.options.vimeoAccount : 0,
                hubspot_id: ( this.options.uploadTo === "hubspot") ? this.options.hubspotAccount : 0,
                slideshare_username:  this.options.slideshareAccount.username,
                slideshare_password:  this.options.slideshareAccount.password,
                category: this.userData.form.category.id,
                name: this.userData.form.title,
                desc: this.userData.form.desc || "",
                tags: JSON.stringify(tagsArray),
                privacyStatus:  this.userData.form.privacy.text.toLowerCase(),
                watermark: this.options.watermark
                //watermark_type: this.options.watermark_type,
            };

            $('.preloader').fadeIn();

            return $.ajax({
                url: '/presentoons/publish-video/',
                data: exportParams,
                type: "POST",
                success: function (data) {
                    $('.preloader').fadeOut();
                    if (data.status == 1) {

                        trackEvent(angular.extend(exportParams, {name: 'publish_start'}));
                        try {
                            var publishTo = that.options.uploadTo;
                            trackEventByParams('publish_flow', 'click', publishTo);

                            if (exportParams.download === "on") {
                                ga('set', 'dimension5', "Publish with Download"); //studio publish type
                            } else if (exportParams.youtube === "on") {
                                ga('set', 'dimension5', "Published to YouTube");
                            } else if (exportParams.facebook === "on") {
                                ga('set', 'dimension5', "Published to Facebook");
                            }
                             else if (exportParams.wistia === "on") {
                                ga('set', 'dimension5', "Published to Wistia");
                            } else if (exportParams.vimeo === "on") {
                                ga('set', 'dimension5', "Published to Vimeo");
                            } else if (exportParams.hubspot === "on") {
                                ga('set', 'dimension5', "Published to Hubspot");    
                            } else {
                                ga('set', 'dimension5', "Published Regular");
                            }

                            ga('set', 'dimension6', "Published Studio"); //publish type
                            trackPageView('/vp_published_studio');

                            if (that.options.uploadTo === "mp4") {
                                trackPageView('/vp_downloaded_studio');
                            }

                            trackEventByParams("click", 'publish', exportParams["category"]);
                        } catch (e) {
                            Raven.captureException(e);
                        }

                        try {
                            typeof(unsetStandardUnloadMessage) !== 'undefined' && unsetStandardUnloadMessage && unsetStandardUnloadMessage();
                        }
                        catch (e) {
                            Raven.captureException(e);
                        }
                    }

                    if (data.status == 0) {
                        Raven.captureException(new Error('Failed to publish'), {
                            extra: {
                                id: that.powtoon.hashid,
                                publishTo: that.publishTo
                            }
                        });
                    }
                }
            });
        }

        return {
            publishTo: publishTo,
            powtoon: powtoon,
            mode: mode,
            options: options,
            userData: userData,
            publish: publish,
            init: init,
            category: category
        }
    }
})();
