// Google Enchanced e-commerce
// Author: alex.kashcheyev@initech.co.il

var EC = function() {

  var GA_NOT_PRESENT_MESSAGE =  'Google Analytics object is not present!';
  var PREPEND_TO_LOG_MESSAGE =  'Google Enchanced E-commerce: ';

  function log(message, level) {
    if (typeof level !== 'string') {
      level = '';
    }
    message = PREPEND_TO_LOG_MESSAGE + message;
    switch (level.toUpperCase()) {
      case 'ERROR':
        console.error(message);
        break;
      case 'WARNING':
        console.warn(message);
        break;
      default:
        console.log(message);
        break;
    }
  };

  function checkGa() {
    if (typeof window.ga !== 'undefined') {
      return true;
    } else {
      log(GA_NOT_PRESENT_MESSAGE, 'ERROR');
      return false;
    }
  };

  return {
    checkGa: checkGa,

    addPromo: function(data, event) {
      if (!checkGa()) {
        return;
      }
      if (typeof data === 'object') {
        ga('ec:addPromo', data);
      } else {
        ga('ec:addPromo', {name: data});
      }
      if (typeof event == 'object' && event.eventCategory && event.eventAction) {
        if (!event.hitType) {
          event.hitType = 'event';
        }
        trackEventByParams(event);
      } else {
        // no event sending, just addpromo data
      }
    },

    setAction: function(action, data) {
      if (!checkGa()) {
        return;
      }
      // Checking for presence of data.id if action is 'purchase' or 'refund'
      if ((action === 'purchase' || action === 'refund') && ( typeof data !== 'object' || !data.id)) {
        log('If action is "purchase" or "refund", id should be provided', 'ERROR');
        return;
      } else {
        if (typeof data === 'object') {
          ga('ec:setAction', action, data);
        } else {
          ga('ec:setAction', action);
        }
      }
    },

    addProduct: function(data) {
      if (!checkGa()) {
        return;
      }
      // Checking for presence of data itself and id and/or name inside it
      if (typeof data !== 'object' || (typeof data.id === 'undefined' && typeof data.name === 'undefined') || (!data.id && !data.name)){
        log('To add product you have to provide its id and/or name', 'ERROR');
        return;
      } else {
        ga('ec:addProduct', data);
      }
    },

    addImpression: function(data) {
      if (!checkGa()) {
        return;
      }
      // Checking if data is present and correct
      if ('object' != typeof data) {
        log('addImpression function should get an object', 'ERROR');
      } else {
        if (!data.id && !data.name) {
          log('Product ID and/or product name should be set', 'ERROR');
        } else {
          ga('ec:addImpression', data);
        }
      }
    },

    clear: function(data) {
      if (!checkGa()) {
        return;
      }
      ga('ecommerce:clear');
    }
  }
}()
