var paymentPopups = function(){
  function showPricingPage(showLoading) {

    if (typeof loadingModal !== 'undefined' && showLoading) {
      loadingModal.open();
    }

    return $.ajax({
      method: 'GET',
      url: '/pricing/popup',
      success: function (data) {
        pop_container(data);

        if (typeof loadingModal !== 'undefined' && showLoading) {
          loadingModal.close();
        }
        try {
          PowtoonApp.hidePreloader();
        } catch (e) {
        }
      }
    });
  }

    var PaymentIframe = {
      overlayClass: 'modal-backdrop-payment',
      modalID: 'purchase__iframe',
      modalClass: 'center-me',
      initTop: function () {
        /* calculate top of the popup in case its a popup */
        var container = this.modalElement;
        if (container) {
          var center = $(window).height() / 2;
          var popup_height = this.modalElement.height();
          var calculated_top = center - popup_height / 2;
          container.css({top: calculated_top > 0 ? calculated_top : 0});
          return calculated_top;
        }
      },
      init: function () { // Generate the html needed with basic css for the component
        var obj = this;
        var closeModal = function () {
          obj.iframeElement.attr('src', '');
          obj.modalElement.css({display: 'none'});
          obj.overlayElement.addClass('hide');
          try {
            PowtoonApp.hidePreloader();
          } catch (e) {
          }
        };
        this.overlayElement = $('<div/>').attr({
          class: obj.overlayClass + ' hide'
        }).appendTo('body').click(closeModal);

        this.modalElement = $('<div><em class="pt-close"/><iframe /></div>')
          .attr({
            class: obj.modalClass,
            border: '0',
            id: obj.modalID
          }).css({
            display: 'none',
            width: 1150,
            height: 670,
            position: "fixed",
            zIndex: 99999,
            top: "10%",
            left: "5%",
            right: "5%",
            margin: "0 auto"
          }).appendTo('body');

        this.iframeElement = this.modalElement.find('iframe').attr({
          id: 'PaymentFormIframe',
          width: '1150',
          border: 0,
          frameborder: 0,
          allowfullscreen: true,
          height: '668',
        }).css({overflow: 'hidden', width: 1150, height: 668});

        this.modalElement.find('.pt-close').click(closeModal);

        this.initTop();
      },

      embedPayment: function (code, url) {
        if (this.modalElement) {
          this.iframeElement.attr({src: url});
          this.modalElement.css({display: 'block'});
          this.overlayElement.removeClass('hide');
        }

      },

      embedPricingTable: function (url) {
        if (this.modalElement) {
          this.iframeElement.attr({src: url});
          this.modalElement.css({display: 'block'});
          this.overlayElement.removeClass('hide');
        }

      }
    };
  var isIE = !!document.documentMode

  function showBusinessPurchase(){
    if (!isIE) {
      PaymentIframe.init();
      PaymentIframe.initTop();
      PaymentIframe.embedPayment('Business', '/purchase/business_monthly/embed');
      // making the input fields inside the iframe focused, enabling the IE users to fill the form
      var iframeExists = PaymentIframe.find('iframe');
      if (iframeExists) {
        $(iframeExists).contents().find('input').first().focus().blur();
      }
    }
    else {
        window.open('/purchase/business_monthly','_blank');
    }


  }

  function showPricing(){
    if (!isIE) {
      PaymentIframe.init();
      PaymentIframe.initTop();
      PaymentIframe.embedPricingTable('/pricing');
    }
    else {
      window.open('/pricing','_blank');
    }
  }

  function showBizPlan(){
    if (!isIE) {
      PaymentIframe.init();
      PaymentIframe.initTop();
      PaymentIframe.embedPayment("Business", "/purchase/business_monthly");
    }
    else {
      window.open('/purchase/business_monthly','_blank');
    }
  }


  return {
      showPricingPage: showPricingPage,
      PaymentIframe: PaymentIframe,
      showBusinessPurchase: showBusinessPurchase,
      showPricing: showPricing,
      showBizPlan: showBizPlan
    }
}();

$(document).on('click', '.js-go-to-pricing', function (e) {
  e.stopPropagation();

  paymentPopups.showPricing();

});