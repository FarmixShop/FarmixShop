var SelectBox =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(1);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _classnames = __webpack_require__(2);
	
	var _classnames2 = _interopRequireDefault(_classnames);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var ReactCSSTransitionGroup = _react2.default.addons.CSSTransitionGroup;
	
	var SelectBox = function (_React$Component) {
	    _inherits(SelectBox, _React$Component);
	
	    function SelectBox(props) {
	        _classCallCheck(this, SelectBox);
	
	        var _this = _possibleConstructorReturn(this, (SelectBox.__proto__ || Object.getPrototypeOf(SelectBox)).call(this, props));
	
	        _this.state = {
	            input_value: "",
	            placeholder: "Select a Category...",
	            show_list: false,
	            show_input: true
	        };
	
	        _this.handleOptionClick = _this.handleOptionClick.bind(_this);
	        _this.handlePlaceholderClick = _this.handlePlaceholderClick.bind(_this);
	        _this.clickDocument = _this.clickDocument.bind(_this);
	        _this.toggleDefaultOption = _this.toggleDefaultOption.bind(_this);
	        return _this;
	    }
	
	    _createClass(SelectBox, [{
	        key: 'componentWillReceiveProps',
	        value: function componentWillReceiveProps(nextProps) {
	            if (nextProps.defaultOption) {
	                this.setState({
	                    placeholder: nextProps.defaultOption.text,
	                    input_value: nextProps.defaultOption.text,
	                    show_input: true
	                });
	            }
	        }
	    }, {
	        key: 'render',
	        value: function render() {
	            return _react2.default.createElement(
	                'div',
	                { ref: 'select_component', className: 'selectbox' },
	                _react2.default.createElement(HiddenInput, { value: this.state.input_value }),
	                this.state.show_input ? _react2.default.createElement(SelectInput, { error: this.props.error,
	                    onClick: this.handlePlaceholderClick, placeholder: this.state.placeholder }) : null,
	                this.state.show_list ? _react2.default.createElement(SelectList, {
	                    onClick: this.handleOptionClick,
	                    data: this.props.data,
	                    upgradeCallback: this.props.upgradeCallback,
	                    upgradeEditorCallback: this.props.upgradeEditorCallback
	                }) : null
	            );
	        }
	    }, {
	        key: 'handleOptionClick',
	        value: function handleOptionClick(id, tag) {
	
	            this.setState({
	                placeholder: tag,
	                input_value: tag,
	                show_list: false,
	                show_input: true
	            });
	
	            if (this.props.onChangeCallback) {
	                this.props.onChangeCallback.call({ text: tag, id: id });
	            }
	        }
	    }, {
	        key: 'handlePlaceholderClick',
	        value: function handlePlaceholderClick() {
	            this.setState({
	                show_list: true,
	                show_input: false
	            });
	        }
	    }, {
	        key: 'toggleDefaultOption',
	        value: function toggleDefaultOption() {
	            if (this.props.defaultOption && this.props.defaultOption.text !== this.state.input_value) {
	                this.setState({
	                    placeholder: this.props.defaultOption.text,
	                    input_value: this.props.defaultOption.text,
	                    show_input: true
	                });
	            }
	        }
	    }, {
	        key: 'clickDocument',
	        value: function clickDocument(e) {
	            var component = _react2.default.findDOMNode(this.refs.select_component);
	            if (e.target == component || $(component).has(e.target).length) {
	                // Todo later
	            } else {
	                this.setState({
	                    show_list: false,
	                    show_input: true
	                });
	            }
	        }
	    }, {
	        key: 'componentDidMount',
	        value: function componentDidMount() {
	            $(document).bind('click', this.clickDocument);
	
	            this.toggleDefaultOption();
	        }
	    }, {
	        key: 'componentWillUnmount',
	        value: function componentWillUnmount() {
	            $(document).unbind('click', this.clickDocument);
	        }
	    }]);
	
	    return SelectBox;
	}(_react2.default.Component);
	
	var SelectList = function (_React$Component2) {
	    _inherits(SelectList, _React$Component2);
	
	    function SelectList() {
	        _classCallCheck(this, SelectList);
	
	        return _possibleConstructorReturn(this, (SelectList.__proto__ || Object.getPrototypeOf(SelectList)).apply(this, arguments));
	    }
	
	    _createClass(SelectList, [{
	        key: 'render',
	        value: function render() {
	            var that = this;
	
	            var optionNodes = this.props.data.map(function (option) {
	                var props = {
	                    text: option.text,
	                    id: option.id,
	                    disabled: option.disabled,
	                    premium: option.premium,
	                    onClick: that.props.onClick,
	                    comingSoon: option.comingSoon,
	                    upgradeCallback: that.props.upgradeCallback,
	                    upgradeEditorCallback: that.props.upgradeEditorCallback
	                };
	
	                return _react2.default.createElement(Option, props);
	            });
	
	            return _react2.default.createElement(
	                ReactCSSTransitionGroup,
	                { transitionEnterTimeout: 500, transitionAppear: true, transitionName: 'example' },
	                _react2.default.createElement(
	                    'div',
	                    { className: 'select_list' },
	                    optionNodes
	                )
	            );
	        }
	    }]);
	
	    return SelectList;
	}(_react2.default.Component);
	
	var Tooltip = _react2.default.createClass({
	    displayName: 'Tooltip',
	
	    render: function render() {
	        return _react2.default.createElement(
	            'div',
	            { className: 'rebirth-tooltip' },
	            _react2.default.createElement(
	                'span',
	                null,
	                this.props.tooltip_text
	            )
	        );
	    }
	});
	
	var PremiumBadge = function (_React$Component3) {
	    _inherits(PremiumBadge, _React$Component3);
	
	    function PremiumBadge(props) {
	        _classCallCheck(this, PremiumBadge);
	
	        var _this3 = _possibleConstructorReturn(this, (PremiumBadge.__proto__ || Object.getPrototypeOf(PremiumBadge)).call(this, props));
	
	        _this3.upgrade = _this3.upgrade.bind(_this3);
	        return _this3;
	    }
	
	    _createClass(PremiumBadge, [{
	        key: 'render',
	        value: function render() {
	            var premiumBadgeClasses = (0, _classnames2.default)({
	                'selectbox-premium-badge': true,
	                'feature-popup-hover-element': this.props.hover
	            });
	
	            return _react2.default.createElement(
	                'div',
	                { className: premiumBadgeClasses },
	                _react2.default.createElement(
	                    'div',
	                    { className: 'feature-popup-container' },
	                    _react2.default.createElement(
	                        'div',
	                        { className: 'feature-popup-one-button' },
	                        _react2.default.createElement(
	                            'p',
	                            null,
	                            'Upgrade to one of the Premium plans in order to use this feature'
	                        ),
	                        _react2.default.createElement(
	                            'a',
	                            { className: 'button button-alert small', onClick: this.upgrade },
	                            'Upgrade'
	                        )
	                    )
	                ),
	                _react2.default.createElement('div', { className: 'premium_badge_img' })
	            );
	        }
	    }, {
	        key: 'upgrade',
	        value: function upgrade() {
	            var that = this;
	
	            if (typeof EC !== "undefined" && this.props.source && publish_source) {
	                EC.addPromo({ id: 'Publish_flow', name: 'Upgrade', creative: this.props.source, position: 'publish_modal' });
	                EC.setAction("promo_click");
	                trackEventByParams("upgrade_to_" + this.props.source, "upgrade", "publish_flow_" + publish_source);
	            }
	
	            if (typeof editor_type !== 'undefined') {
	                if (this.props.type === 'pro') {
	                    if (typeof paymentPopups !== 'undefined') {
	                        window.location.hash = '/Publish/Loading';
	                        paymentPopups.showPricing().success(function () {
	                            !that.props.upgradeEditorCallback || that.props.upgradeEditorCallback();
	                        });
	                    } else {
	                        return false;
	                    }
	                } else {
	                    if (typeof paymentPopups !== 'undefined') {
	                        paymentPopups.showBusinessPurchase();
	                        !that.props.upgradeEditorCallback || that.props.upgradeEditorCallback();
	                    } else {
	                        return false;
	                    }
	                }
	            } else {
	                if (this.props.type === 'pro') {
	                    window.open('/pricing', '_blank');
	                } else {
	                    window.open('/purchase/business_monthly/', '_blank');
	                }
	
	                !that.props.upgradeCallback || that.props.upgradeCallback();
	            }
	        }
	    }]);
	
	    return PremiumBadge;
	}(_react2.default.Component);
	
	var Option = function (_React$Component4) {
	    _inherits(Option, _React$Component4);
	
	    function Option(props) {
	        _classCallCheck(this, Option);
	
	        var _this4 = _possibleConstructorReturn(this, (Option.__proto__ || Object.getPrototypeOf(Option)).call(this, props));
	
	        _this4.handleClick = _this4.handleClick.bind(_this4);
	        return _this4;
	    }
	
	    _createClass(Option, [{
	        key: 'render',
	        value: function render() {
	
	            // Enable hover if the option is premium and the user cant use it.
	            var hover = this.props.premium && this.props.disabled;
	
	            var optionClasses = (0, _classnames2.default)({
	                'option': true,
	                'disabled': this.props.disabled,
	                'coming-soon': this.props.comingSoon,
	                'feature-popup-hover-element': hover
	            });
	
	            return _react2.default.createElement(
	                'div',
	                { onClick: this.handleClick, className: optionClasses },
	                _react2.default.createElement(
	                    'h3',
	                    { className: 'title' },
	                    ' ',
	                    this.props.text,
	                    ' '
	                ),
	                this.props.premium ? _react2.default.createElement(PremiumBadge, {
	                    hover: hover,
	                    type: 'pro',
	                    source: this.props.text,
	                    upgradeEditorCallback: this.props.upgradeEditorCallback,
	                    upgradeCallback: this.props.upgradeCallback
	                }) : null
	            );
	        }
	    }, {
	        key: 'handleClick',
	        value: function handleClick() {
	            if (!this.props.disabled && !this.props.comingSoon) {
	                this.props.onClick(this.props.id, this.props.text);
	            }
	        }
	    }]);
	
	    return Option;
	}(_react2.default.Component);
	
	var SelectInput = _react2.default.createClass({
	    displayName: 'SelectInput',
	
	
	    render: function render() {
	        var inputClasses = (0, _classnames2.default)({
	            'select_input': true,
	            'tooltip-anchor': true,
	            'error': this.props.error
	        });
	        return _react2.default.createElement(
	            'div',
	            { onClick: this.props.onClick, className: inputClasses },
	            this.props.error ? _react2.default.createElement(Tooltip, { tooltip_text: this.props.error }) : null,
	            _react2.default.createElement(
	                'h2',
	                null,
	                this.props.placeholder
	            ),
	            _react2.default.createElement('div', { className: 'select-input-icon pticon-arrowlevel' })
	        );
	    }
	});
	
	var HiddenInput = function (_React$Component5) {
	    _inherits(HiddenInput, _React$Component5);
	
	    function HiddenInput(props) {
	        _classCallCheck(this, HiddenInput);
	
	        return _possibleConstructorReturn(this, (HiddenInput.__proto__ || Object.getPrototypeOf(HiddenInput)).call(this, props));
	    }
	
	    _createClass(HiddenInput, [{
	        key: 'render',
	        value: function render() {
	            var value = this.props.value;
	            return _react2.default.createElement('input', { type: 'hidden', className: 'select_input_value', value: this.props.value });
	        }
	    }]);
	
	    return HiddenInput;
	}(_react2.default.Component);
	
	module.exports = SelectBox;

/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = React;

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
	  Copyright (c) 2016 Jed Watson.
	  Licensed under the MIT License (MIT), see
	  http://jedwatson.github.io/classnames
	*/
	/* global define */
	
	(function () {
		'use strict';
	
		var hasOwn = {}.hasOwnProperty;
	
		function classNames () {
			var classes = [];
	
			for (var i = 0; i < arguments.length; i++) {
				var arg = arguments[i];
				if (!arg) continue;
	
				var argType = typeof arg;
	
				if (argType === 'string' || argType === 'number') {
					classes.push(arg);
				} else if (Array.isArray(arg)) {
					classes.push(classNames.apply(null, arg));
				} else if (argType === 'object') {
					for (var key in arg) {
						if (hasOwn.call(arg, key) && arg[key]) {
							classes.push(key);
						}
					}
				}
			}
	
			return classes.join(' ');
		}
	
		if (typeof module !== 'undefined' && module.exports) {
			module.exports = classNames;
		} else if (true) {
			// register as 'classnames', consistent with npm package name
			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function () {
				return classNames;
			}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
		} else {
			window.classNames = classNames;
		}
	}());


/***/ }
/******/ ]);
//# sourceMappingURL=SelectBox.bundle.js.map