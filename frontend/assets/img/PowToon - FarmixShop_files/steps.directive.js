// React and ReactDOM are a must for this.

(function () {
    'use strict';

    angular.module('publishModalApp').directive('steps', steps);

    function steps() {
        var directive = {
            link: link,
            template: '<span id="steps-anchor"></span>',
            restrict: 'E',
            scope: {
                steps: '=',
                current: '='
            }
        };

        function link(scope, element, attrs) {
            var StepsComponent;

            scope.$watchCollection('[steps, current]', function(){
                if (scope.current || scope.steps) {
                    if (!StepsComponent) {
                        StepsComponent = ReactDOM.render(
                            React.createElement(
                                Steps,
                                {
                                    steps: scope.steps,
                                    current: scope.current
                                }
                            ),
                            element.find('#steps-anchor')[0]);
                    } else {
                        StepsComponent.setState({current: scope.current, steps: scope.steps});
                    }
                }
            });
        }

        return directive;
    }
})();