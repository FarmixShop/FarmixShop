/* Powtoon Share Plugin Limited */
/* v 0.1.0 */

$(function() {
    var socialShare = {
        height:450,
        width:550,
        dataClass:'share',
        btnClass:'.social-icon:not(.not-allowed)',
        share_url : 'www.powtoon.com',
        twitter:{
            url:'https://twitter.com/intent/tweet?',
            slug:'t'
        },
        "in":{
            url:'http://www.linkedin.com/shareArticle?&ro=false&mini=true&',
            slug:'l'
        },
        linkedin:{
            url:'http://www.linkedin.com/shareArticle?&ro=false&mini=true&',
            slug:'l'
        },
        facebook:{
            url:'http://www.facebook.com/sharer.php?',
            slug:'f'
        },
        gplus:{
            url:'https://plus.google.com/share?',
            slug:'g'
        },
        google:{
            url:'https://plus.google.com/share?',
            slug:'g'
        },
        setDimensions: function() {
            this.top = ($(window).height() / 2) - 225;
            this.left = ($(window).width() / 2) - 225;
        },
        getDimUrl: function() {
            return 'height=' + this.height + ', width=' + this.width + ', top=' + this.top + ', left='
                             + this.left + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0';
        },
        init: function(url, hash_id){
            this.share_url = url;

            var hash = typeof hash_id !== "undefined" ? hash_id : url.split("show")[1].replace(/\//g, '');

            this.setDimensions();
            $(this.btnClass).on('click',function(e){
                var $this = $(this);
                var data = $this.data();
                var dc = socialShare.dataClass;
                var params = {};
                for(i in data) {
                    if(dc == i.substring(0,dc.length)){
                        attr = i.substring(dc.length).toLowerCase();
                        params[attr] = data[i];
                    }
                }

                $.ajax({
                    url: '/presentoons/ajax/track-sharing/',
                    data: $.extend({
                        hash_id: hash_id
                    }, params)
                });

                e.preventDefault();
                socialShare.sharePage(params, hash);
            });
        },
        sharePage:function(p, hash){
            vendor = (typeof p.vendor !== "undefined") ? p.vendor : "facebook";
            var url_pre = (vendor === 'facebook') ? 'u' : 'url';

            /**
             * window.getPlayerMode is used here as it defined in an async script
             * that may load after this script is executed
             */
            p[url_pre] = window.location.protocol + "//" + window.location.hostname + "/"
                            + this[vendor].slug +  "/" + hash + "/" + window.userPresentoonRelCode
                            + "/" + (window.getPlayerMode() === 'movie' ? 'm' : 'p');
            var url = this[vendor].url + this.objToUrl(p);

            p[name] = p.name || "powtoon";
            this.openShareDialog(url,p[name],this.getDimUrl());
        },
        openShareDialog:function(url,name,p){
            window.open(url,name,p);
        },
        objToUrl:function(obj){
            return Object.keys(obj).map(function(key){
                return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]);
            }).join('&');
        }
    };

    function shareEmail() {
        /**
         * window.getPlayerMode is used here as it defined in an async script
         * that may load after this script is executed
         */
        var url = window.emailShareUrlTemplate.replace('<% mode %>', (window.getPlayerMode() === 'movie' ? 'm' : 'p'));
        window.open(url);
    }

    /* Exports */

    window.shareEmail = shareEmail;
    window.socialShare = socialShare;
});


