(function () {
    'use strict';

    angular.module('publishModalApp')
        .controller('defaultController', defaultController);

    defaultController.$inject = ['$scope'];
    function defaultController($scope) {
        $scope.closePublishFlow('default');
    }
})();