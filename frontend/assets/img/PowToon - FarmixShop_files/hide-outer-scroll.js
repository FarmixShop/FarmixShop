/**
 * Hides outer scrollbar as in all pages that uses the publish modal as all of those pages has
 * scrollbar (overflow) style on their html element, this scrollbar is shown for small windows
 * alongside with the modal scrollbar which is bad UX.
 * This script removes the outer scrollbar when the publish modal is opened and restores it
 * when it closes.
 */
$(function() {
    $(document)
        .on('publish-modal-close', function() {
            $('html').css('overflow-y', 'auto'); // restore entire page scroll when model closes
        })
        .on('publish-modal-open', function () {
            $('html').css('overflow-y', 'hidden'); // disable entire page scroll when modal is opened
        });
});