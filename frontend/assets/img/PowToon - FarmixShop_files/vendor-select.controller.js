angular.module('publishModalApp')
    .controller('VendorSelectController', VendorSelectController);

VendorSelectController.$inject = ['$scope', '$location', 'mainService'];
function VendorSelectController($scope, $location, s) {
    function setNextLabelByVendor() {
        switch (s.exportOptions.uploadTo) {
            case "youtube":
            case "hubspot":
            case "wistia":
            case "vimeo":
            case "slideshare":
                $scope.$parent.modal.nextLabel = "Continue";
                break;
            default:
                $scope.$parent.modal.nextLabel = (s.userData.plan.published === true) ? "Re-Publish" : "Publish";
        }
    }

    angular.extend($scope.$parent.modal, {
        nextEnabled: true,
        backEnabled: true,
        backLabel: "Back",
        closeEnabled: true,
        step: "step1",
        stepHeadline: $scope.$parent.getDefaultPublishHeadline(''),
        next: function () {
            switch ($scope.exportOptions.uploadTo) {
                case "youtube":
                    $location.path($scope.$parent.routes.step_youtube);
                    break;
                case "wistia":
                    $location.path($scope.$parent.routes.step_wistia);
                    break;
                case "vimeo":
                    $location.path($scope.$parent.routes.step_vimeo);
                    break;
                case "hubspot":
                    $location.path($scope.$parent.routes.step_hubspot);
                    break;
                case "slideshare":
                    $location.path($scope.$parent.routes.step_slideshare);
                    break;
                default:
                    $scope.export();
            }
            s.settings.isMoreOptionsCollapsed = $scope.isMoreOptionsCollapsed; // save current 'more options' checkbox state in the service
            $scope.$parent.modal.steps.push('vendorSelect');

        },
        back: function () {
            s.settings.isMoreOptionsCollapsed = $scope.isMoreOptionsCollapsed; // save current 'more options' checkbox state in the service
            $location.path($scope.$parent.routes.step_quality);
        }
    });
    $scope.mode = s.mode;
    setNextLabelByVendor();

    $scope.isMoreOptionsCollapsed = s.settings.isMoreOptionsCollapsed; // load 'more options' checkbox state from service

    $scope.unlist = s.exportOptions.unlist;
    $scope.benefits = [
        "Easy social sharing (<span class='boldDax'>Facebook</span>, Google +, Twitter and more…)",
        "SEO optimized",
        "Mobile Compatible"
    ];

    $scope.uploadVendor = s.exportOptions.uploadTo;

    $scope.exportOptions = s.exportOptions;

    switch (s.mode) {
        case 'Movie':
            $scope.uploadVendors = s.vendorsMovie;
            break;
        case 'Presentation':
            $scope.uploadVendors = s.vendorsPresentation;
            break;
    }

    $scope.thumb = "";

    $scope.getThumb = function () {

        if (!s.userData.thumb.length) {
            if (s.userData.flashData.thumb_url) {
                $scope.thumb = s.userData.flashData.thumb_url;
            }
        }
    };
    $scope.isPremium = s.userData.isPremium;
    $scope.checkIndexed = function () {
        if (!s.userData.indexed) {
            s.setUnlist(!$scope.unlist);
            return;
        } else {
            s.setUnlist(false);
            $scope.unlist = false;
            return false;
        }
    };
    $scope.setWatermark = function () {
        s.setWatermark($scope.$parent.watermark);
        if (!s.userData.watermark) {
            s.setWatermark($scope.$parent.watermark);
            return;
        } else {
            s.setWatermark(false);
            $scope.noWatermark = false;
            return false;
        }
    };
    $scope.checkPremium = function () {
        $scope.$parent.modal.before = $scope.$parent.routes.step_vendor;
        if (s.userData.isPremium) {
            s.setUnlist(!$scope.unlist);
            return;
        }
        else {

            s.setUnlist(false);
            $scope.unlist = false;
            $location.path($scope.$parent.routes.step_upgrade);
            return false;
        }
    };

    $scope.setUploadVendor = function (val) {
        if (val == s.exportOptions.uploadTo) {
            $('input[type="radio"]').filter('[value=\"' + val + '\"]').prop("checked", false);
            s.setUploadVendor(false);
            $location.path($scope.$parent.routes.step_vendor);
        }
        else {
            $scope.$parent.modal.before = $scope.$parent.routes.step_vendor;
            s.setUploadVendor(val);
        }
        setNextLabelByVendor();
    }

    // TODO: Rewrite to not stupid
    setTimeout(function () {
        $scope.setUploadVendor($location.search().vendor);
        s.exportOptions.uploadTo = $location.search().vendor;
        $scope.uploadVendor = $location.search().vendor;

        $scope.$apply();
    }, 100);
};
