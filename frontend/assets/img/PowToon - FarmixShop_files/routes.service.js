(function () {
    angular.module('publishModalApp')
        .factory('Steps', Steps);

    function Steps() {
        var routes = {
            start: {
                next: {
                    youtube: 'youtubeAuth',
                    facebookupload: 'facebookAuth',
                    wistia: 'wistiaAuth',
                    vimeo: 'vimeoAuth',
                    hubspot: 'hubspotAuth',
                    slideshare: 'slideshareAuth',
                    otherwise: 'details'
                }
            },
            facebookAuth: {
                next: {
                    otherwise: 'details'
                },
                path: '/Publish/FacebookAccount',
                title: 'Type'
            },
            youtubeAuth: {
                next: {
                    otherwise: 'details'
                },
                path: '/Publish/YoutubeAccount',
                title: 'Type'
            },
            wistiaAuth: {
                next: {
                    otherwise: 'details'
                },
                path: '/Publish/WistiaAccount',
                title: 'Type'
            },
            vimeoAuth: {
                next: {
                    otherwise: 'details'
                },
                path: '/Publish/VimeoAccount',
                title: 'Type'
            },
            hubspotAuth: {
                next: {
                    otherwise: 'details'
                },
                path: '/Publish/HubspotAccount',
                title: 'Type'
            },
            slideshareAuth: {
                next: {
                    otherwise: 'details'
                },
                path: '/Publish/SlideshareAccount',
                title: 'Type'
            },
            details: {
                next: {
                    otherwise: 'quality'
                },
                path: '/Publish/Details',
                title: 'Details'
            },
            quality: {
                path: '/Publish/Quality',
                title: 'Quality'
            }
        };

        var route;

        function getRoute(target, currentStep){
            if (!currentStep){
                route = [];
                currentStep = routes.start;
            }

            if (!currentStep.next) {
                return route;
            } else {
                if (currentStep.next[target]) {
                    currentStep = routes[currentStep.next[target]];
                } else {
                    currentStep = routes[currentStep.next.otherwise];
                }

                route.push({
                    path: currentStep.path,
                    title: currentStep.title
                });
                return getRoute(target, currentStep);
            }
        }

        return {
            getRoute: getRoute
        }
    }
})();