// Blog links color change
$('.blog_popup_main').hover(function(){
   $('span.menu-blog').css('color','#42a5f5');
  },function(){
    $('span.menu-blog').css('color','#253740');
    $('#blog-link').on('mouseover',function(){
      $('span.menu-blog').css('color','#42a5f5');
    });
    $('#blog-link').on('mouseout',function(){
      $('span.menu-blog').css('color','#253740');
    });
  }
);

// When blog popup is open close the dropdown menu
$('#blog-link').on('mouseover',function(){
  $('.navigation ul li.open').removeClass('open');
});


$('.click-head').click(function() {
  $(this).next('ul').slideToggle();
  $(this).toggleClass('openFooterLink');
})

$('.mobile-menu').click(function() {
  $('.nav-block').addClass('slide-right')
})

$('.close-btn').click(function() {
  $('.nav-block').removeClass('slide-right')
})
$(window).scroll(function() {
  if ($(window).scrollTop() > 0) {
    $('.header').addClass('fix');
  } else {
    $('.header').removeClass('fix');
  }
})