angular.module('publishModalApp')
    .controller('SlideshareController', SlideshareController);

SlideshareController.$inject = ['$scope', 'mainService', 'Publish'];
function SlideshareController($scope, s, Publish) {


    $scope.username = Publish.options.slideshareAccount.username || '';
    $scope.password = Publish.options.slideshareAccount.password || '';

    $scope.$parent.validate = function () {
        // Client side form validation
        $scope.state = 'validatingForm';
        var isValidatedForm = validateForm();
        if (isValidatedForm) {
            // Checks for errors
            s.validateSlideshareAccount($scope.username, $scope.password, function callback(state) {
                switch (state) {
                    case 'accepted':
                        $scope.$parent.validated = true;
                        $scope.state = 'authAccepted';
                        break;
                    case 'rejected':
                        $scope.$parent.validated = false;
                        $scope.state = 'authRejected';
                        break;
                    default :
                        $scope.state = 'unexpectedError';
                }
            });
        }
    };

    $scope.next = function () {
        $scope.modal.next();
    };

    function validateForm() {
        $scope.errMsg = '';
        $scope.invalidUsername = false;

        if ($scope.username && $scope.password) {
            // Slideshare username validation
            if (/^([\w])+([\w\-])*([\w])+$/.test($scope.username) === false) {
                $scope.invalidUsername = true;
                return false;
            }

        } else {
            $scope.errMsg = 'Username and Password required.';
            return false;
        }

        return true;
    }

    $scope.state = 'standby';
    $scope.invalidUsername = false;
}

