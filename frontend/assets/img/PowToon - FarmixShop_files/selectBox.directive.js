// React and ReactDOM are a must for this.

(function () {
    'use strict';

    angular.module('publishModalApp').directive('selectBox', selectBox);

    function selectBox() {
        var directive = {
            link: link,
            template: '<div class="select-box-anchor"></div>',
            restrict: 'E',
            scope: {
                options: '=',
                selected: '=',
                error: '=',
                upgradeCallback: '=',
                upgradeEditorCallback: '='
            }
        };

        function link(scope, element, attrs) {

            if (scope.selected.id === 0 && !scope.selected.text) {
                scope.selected = scope.options[0];
            }

            renderSelectBox();

            scope.$watch('error', function () {
                renderSelectBox();
            });

            scope.$watch('selected', function (newVal, oldVal) {
                if (newVal && oldVal && newVal.id !== oldVal.id) {
                    renderSelectBox();
                }
            });

            function renderSelectBox() {
                ReactDOM.render(
                    React.createElement(
                        SelectBox,
                        {
                            data: scope.options,
                            error: scope.error,
                            defaultOption: scope.selected,
                            upgradeCallback: scope.upgradeCallback,
                            upgradeEditorCallback: scope.upgradeEditorCallback,
                            onChangeCallback: function () {
                                // The data attribute in the SelectBox saves the id as a String. -___-
                                // Even though i think it should not.
                                scope.selected = {id: parseInt(this.id), text: this.text};
                                scope.error ? scope.error = '' : null;
                                scope.$apply();
                            }
                        }
                    ),
                    element.find('.select-box-anchor')[0]);
            }
        }

        return directive;
    }
})();