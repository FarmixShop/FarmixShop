angular.module('publishModalApp')
    .controller('DetailsController', DetailsController);

DetailsController.$inject = ['$scope', 'mainService', 'Publish'];
function DetailsController($scope, s, Publish) {

    $scope.mode = s.mode;
    $scope.options = Publish.options;
    $scope.form = Publish.userData.form;
    $scope.selectedVendor = Publish.options.uploadTo;
    $scope.videoCategories = s.videoCategories;
    $scope.errMsg = "";
    $scope.validation = true;
    $scope.videoRestrictions = s.videoRestrictions;
    $scope.$parent.validate = validate;
    cleanErrors();

    $scope.$on('close', function () {
        cleanErrors();
    });

    function cleanErrors() {
        $scope.categoryError = '';
        $scope.errMsg = '';
    }

    $scope.$watch('form.category.id', function(category, oldVal){
        if (category.id && category.id !== 0) {
            validateCategory();
        }
    });

    function validate() {
        cleanErrors();
        $scope.errMsg = "";
        var tags = $scope.form.tags.length ? $scope.form.tags.split(',').join(",").split(',') : [];

        if (!validateCategory()) {
            $scope.$parent.validated = false;
            $scope.categoryError = 'Please select a category';
        } else if (!$scope.form.title) {
            $scope.$parent.validated = false;
        } else if (tags.length > 10) {
            $scope.$parent.validated = false;
            $scope.errMsg = "There is a maximum of 10 tags";
        } else {
            $scope.$parent.validated = true;
        }
    }

    function validateCategory() {
        if (typeof $scope.form.category === "undefined" || $scope.form.category.id === $scope.videoCategories[0].id) {
            $scope.categoryError = 'Please select a category';
            return false;
        } else {
            $scope.categoryError = '';
            return true;
        }
    }
}