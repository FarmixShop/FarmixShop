$(document).on('publish_completed', function(e, data) {

  if (data.error) {
    showSimpleNotification({message: 'Sorry, we could not export your Powtoon, Please try again.'})
  } else {

    var target = data.target;
    var presentoonName = data.presentoon_name;
    var externalUrl = data.external_url;
    var category = data.category;
    var downloadUrl = data.download_url;
    var powtoonPageUrl = data.powtoon_page_url;

    var message = '';

    // Make the first letter uppercase
    var pretty_target_name = _s.toStudlyCaps(target);
    if (pretty_target_name === 'Hubspot') {
      pretty_target_name = 'HubSpot';
    }

    if (category === 'partner') {
      message = _s.format('Your Powtoon "{0}" was passed to {1}', [presentoonName, target]);

    } else if (category === 'download') {
      var fileType = getFileTypeName(target);

      message = 'Your Powtoon has been successfully published. Click <a href="{0}" target="_blank" download>here</a> to Download the {1} File, or  <a href="/my-powtoons/">here</a> to view it in “My Powtoons"';
      message = _s.format(message, [downloadUrl, fileType]);

    } else if (category === 'upload') {
      message = 'Your Powtoon has been successfully published. Click <a href="{0}" target="_blank">here</a> to view it on {1}, or <a href="/my-powtoons/">here</a> to view it in “My Powtoons"';
      message = _s.format(message, [externalUrl, pretty_target_name]);

    } else if (category === 'publish') {
      message = 'Your Powtoon has been successfully published. Click <a href="/my-powtoons/">here</a> to view it in “My Powtoons"'
    }

    showSimpleNotification({message: message});

    function getFileTypeName(target) {
      typesMap = {
        'pdf': 'PDF',
        'ppt': 'Power Point',
        'mp4': 'MP4',
        'offline_player': 'Offline'
      };

      return typesMap[target];
    }
  }
});
