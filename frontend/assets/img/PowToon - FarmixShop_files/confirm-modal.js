(function () {
    'use strict';

    var template = $(
            '<div class="powtoon-rebirth modal" id="confirm-modal" >' +
                '<div class="system-popup confirm-modal">' +
                    '<div>' +
                        '<section class="content" style="padding-bottom: 36px">' +
                        '<h2> </h2>' +
                        '</section>' +
                        '<footer class="pticon-modal-footer">' +
                            '<a id="confirm-modal-yes" class="button button-hover button-small" style="margin-right: 10px;">Export</a>' +
                            '<a id="confirm-modal-no" class="button button-hover button-small">Cancel</a>' +
                        '</footer>' +
                    '</div>' +
                '</div>' +
            '</div>'
        );

    $(function () {
        $('body').prepend(template);

        window.openConfirmModal = function(html, callbackConfirm, callbackDeny) {
            var $confirmModal = $('#confirm-modal');

            $confirmModal.find('section.content h2').html(html);
            $confirmModal.modal({backdrop: false});

            $('#confirm-modal-yes').one('click', function () {
                if (typeof callbackConfirm === 'function') {
                    callbackConfirm();
                }

                $confirmModal.modal('hide');
            });

            $('#confirm-modal-no').one('click', function () {
                if (typeof callbackDeny === 'function') {
                    callbackDeny();
                }

                $confirmModal.modal('hide');
                if (window.frameElement !== null && window.frameElement.id === 'publishIframe') {
                    window.parent.closePublishIframe();
                }
            });
        };

    });

})();