(function () {
    'use strict';

    var template = $(
        '<div class="powtoon-rebirth modal" id="loading-modal" >' +
        '<div class="system-popup loading-modal">' +
        '<div>' +
        '<div class="content" style="padding-bottom: 36px">' +
        '<canvas width="250" height="200"> </canvas>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>'
    );

    $(function () {
        $('body').prepend(template);

        window.loadingModal = function () {
            var loadingModal = $('#loading-modal');

            var isOpened = false;

            function open() {
                isOpened = true;
                loadingModal.modal({backdrop: false});

                var canvas = loadingModal.find('.content canvas')[0];
                var stage, exportRoot;

                init();

                function init() {
                    exportRoot = new lib.preloaderHtmlfile();

                    stage = new createjs.Stage(canvas);
                    stage.addChild(exportRoot);
                    stage.update();

                    createjs.Ticker.setFPS(lib.properties.fps);
                    createjs.Ticker.addEventListener("tick", stage);
                }
            }


            function close() {
                loadingModal.modal('hide');
                isOpened = false;
            }

            return {
                open: open,
                close: close,
                isOpened: isOpened
            };

        }();
    });

})();