(function () {
  // A notification about expired email.
  var url = Qurl.create();

  if (url.query('email_confirm_expired')) {
    url.query('email_confirm_expired', false);

    var message = 'Your confirmation link is not valid anymore ' +
      '<a href="javascript:void(0)" data-dismiss="alert" class="js-resend-confirm-email"> Click here </a> to get a new one.';
    showSimpleNotification({message: message});

    $(document).on('click', '.js-resend-confirm-email', function () {
      $.get('/email-confirm/resend').fail(function (res) {
        var message = 'Email could not be sent at the moment, please ' +
          '<a href="javascript:void(0)" data-dismiss="alert" class="js-resend-confirm-email">try again later.</a>';
        showSimpleNotification({message: message});
      })
    })
  }
})();