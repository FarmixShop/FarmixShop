angular.module('publishModalApp')
    .controller('QualityController', QualityController);

QualityController.$inject = ['$scope', '$location', 'mainService', 'Publish', '$timeout'];
function QualityController($scope, $location,s, Publish, $timeout) {

    var popupTimeout;

    $scope.options = Publish.options;
    $scope.userData = Publish.userData;

    $scope.youtubeAccounts = Publish.userData.youtube_accounts;
    $scope.facebookAccounts = Publish.userData.facebook_accounts;
    $scope.quality = Publish.options.quality;
    $scope.qualities = s.exportQualities;
    $scope.chooseQuality = function (val) {
        if (Publish.userData.isAgency) {
            s.setExportQuality(val);
            return;
        }
        else if (Publish.userData.isPremium) {
            if (val == "full_hd" && Publish.userData.plan.quality < 1080) {
                s.setExportQuality("720");
                $location.path($scope.$parent.routes.step_upgrade);

            }
            else {
                s.setExportQuality(val);
            }
        }
        else {//free user
            if (val != '480') {
                s.setExportQuality('480');
                $location.path($scope.$parent.routes.step_upgrade);
                return false;
            }
        }
    };

    $scope.change = function (val) {
        console.log(val);
    };



    $scope.$parent.validate = function() {
        $scope.$parent.validated = true;
    };
};
