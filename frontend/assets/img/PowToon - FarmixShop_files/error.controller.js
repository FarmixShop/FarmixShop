angular.module('publishModalApp')
    .controller('ErrorController', ErrorController);

ErrorController.$inject = ['$scope', 'mainService'];
function ErrorController($scope, s) {
    angular.extend($scope.$parent.modal, {
        closeEnabled: true,
        stepHeadline: "Error",
        nextEnabled: true,
        backEnabled: false,
        nextLabel: "Close"
    });
    $scope.isCollapsed = true;
    $scope.errors = s.errors;
    $scope.$parent.modal.next = function () {
        $('#publish-modal').modal('hide');
    }
};