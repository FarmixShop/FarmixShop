// React and ReactDOM are a must for this.

(function () {
    'use strict';

    angular.module('publishModalApp').directive('textArea', textArea);

    function textArea() {
        var directive = {
            link: link,
            template: '<div class="text-area-anchor"></div>',
            restrict: 'E',
            scope: {
                ngModel: '=',
                placeholder: '@',
                textAreaPlaceholder: '@',
                editPlaceholder: '@'
            }
        };

        function link(scope, element, attrs) {

            var initialValue = scope.ngModel || '';

            ReactDOM.render(
                React.createElement(
                    TextArea,
                    {
                        onChangeCallback: function() {
                            scope.ngModel = this.text
                        },
                        placeholder: scope.placeholder,
                        textAreaPlaceholder: scope.textAreaPlaceholder,
                        editPlaceholder: scope.editPlaceholder,
                        initialValue: initialValue
                    }
                ),
                element.find('.text-area-anchor')[0]);
        }

        return directive;
    }
})();