var embedMaster = {
    options: null,
    cont:'#embed_master_template',
    width:window.powtoonEmbedDefaultPlayerWidth,
    height:window.powtoonEmbedDefaultPlayerHeight,
    player:"powtoon",
    sizeBtn:'.select_size',
    init: function(cont) {
        this.container  = $(this.cont);
        this.textarea   =this.container.find('textarea');
        this.pid        = this.container.data('pid') || "";
        this.player = (typeof Player !="undefined")?Player:this.player;
        this.textarea.html(this.getCode());
        this.initClipboard();
        $(this.sizeBtn).each(function(){
            var btn = $(this);
            var width = btn.data('width');
            var height =(embedMaster.player=="wistia") ? btn.data('extraHeight'):btn.data('height');
            btn.text(width+" X "+height);
        });
        $(this.sizeBtn).click(embedMaster.changeSize);
        this.textarea.on('focus', function() {
            var $this=$(this);
            embedMaster.textarea.select();
            // Work around Chrome's little problem
            $this.mouseup(function() {
                // Prevent further mouseup intervention
                $this.unbind("mouseup");
                return false;
            });
        });
    },
    
    show: function(hashid) {
        // show master for given id
        embedMaster.initClipboard(embedMaster.container);
        trackEvent({
            name: 'embed_master_show',
            presentoon_id: embedMaster.pid
        });
    },
    
    initClipboard: function() {
        // 
        $('.md-modal').find(".copy_to_clipboard").zclip({
            path: '//powtoon-new-static.s3.amazonaws.com/js/ZeroClipboard.swf',
            copy:function(){
                //embedMaster.container.find(".copy_to_clipboard").addClass('active');
                return $(this).parents('.md-section').find('textarea').val();
            },
            beforeCopy: function() {
                //$(this).select();
            },
            afterCopy:function(){
                var $this = $(this);
                $this.addClass('copied');
                setTimeout(function(){$this.removeClass('copied');},1000);
                
            }
        });
    },
    
    changeSize: function() {

        // user select some size
        var me = $(this),
            params = {
                width:  me.data('width'),
                height: me.data('height'),
                hashid: embedMaster.pid
            };
        if (embedMaster.player=="wistia") params.height = me.data('extraHeight');
        embedMaster.container.find('li').removeClass('selected');
        me.parent().addClass('selected');
        html = embedMaster.getCode(params);
        embedMaster.container.find('textarea').val(html);
        return false;
    },
    
    getCode: function(params) {
        if (embedMaster.player=="wistia") {
            embedMaster.height = $(embedMaster.sizeBtn).first().data('extraHeight');
        }
        if(params){
            return '<iframe width=\"'+params.width+'\" height=\"'+params.height+'\" src=\"'+embedMaster.getUrl(embedMaster.pid)+'\" frameborder=\"0\"></iframe>';    
        }
        else{ return '<iframe width=\"'+embedMaster.width+'\" height=\"'+embedMaster.height+'\" src=\"'+embedMaster.getUrl(embedMaster.pid)+'\" frameborder=\"0\"></iframe>';}
        // return code for embede iframe
    },
    
    getUrl: function(hashid) {
        var player_version = window.location.href.match(/html_player_version=([^&]*)/i);
        if (player_version) {
            return 'https://' + embedMaster.options.domain + '/embed/' + hashid + '/' +  '?' + player_version[0];
        }

        return 'https://' + embedMaster.options.domain + '/embed/' + hashid + '/';
    }  
}

