angular.module('publishModalApp')
    .controller('FacebookController', FacebookController);

FacebookController.$inject = ['$scope', 'mainService', 'Publish'];

function FacebookController($scope, s, Publish) {

    $scope.facebookAccounts = Publish.userData.facebook_accounts;
    $scope.facebookUrl = Publish.userData.facebook_url;
    $scope.facebookAccount = Publish.options.facebookAccount;
    $scope.channel = true;

    $(document).unbind('facebook');
    $(document).unbind('retrieve');
    $(document).unbind('loginComplete');
    $(document).unbind('loginError');

    $scope.unauthorized = false;
    $scope.channel = true;
    $scope.privacy = [
        {id: 0, text: 'Public'},
        {id: 1, text: 'Unlisted'},
        {id: 2, text: 'Private'}
    ];

    $scope.userData = Publish.userData;
    $scope.form = Publish.userData.form;

    $scope.$parent.validate = function () {
        if (Publish.options.facebookAccount) {
            $scope.$parent.validated = true;
        }
        else {
            $scope.$parent.validated = false;
        }
    };

    $(document).bind('retrieve.facebook', function () {
        $scope.facebookAccount = Publish.options.facebookAccount;
        $scope.facebookAccounts = Publish.userData.facebook_accounts;
    });

    $scope.addAccount = function () {
        $(window)[0].open("/account/login/facebookupload/", "Login", "width=500,height=700");
        $(document).bind('loginComplete', function () {
            s.getFacebookAccountDetails();
        });
    };

    $scope.removeAccount = function (account) {
        $('.preloader').fadeIn();
        s.deleteFacebookAccount(account).then(function () {
            if ($scope.facebookAccounts.length && account.id === $scope.facebookAccount) {
                $scope.facebookAccount = $scope.facebookAccounts[0].id;
                Publish.options.facebookAccount = $scope.facebookAccounts[0].id;
            } else if ($scope.facebookAccounts.length === 0) {
                $scope.facebookAccount = false;
                Publish.options.facebookAccount = false;
            }
        });

        $scope.channel = true;
    };

    $scope.setFacebookAccount = function (id) {
        $scope.facebookAccount = id;
        s.setFacebookAccount(id);
    }
};
