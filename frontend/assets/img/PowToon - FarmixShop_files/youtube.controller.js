angular.module('publishModalApp')
    .controller('YoutubeController', YoutubeController);

YoutubeController.$inject = ['$scope', 'mainService', 'Publish'];

function YoutubeController($scope, s, Publish) {

    $scope.youtubeAccounts = Publish.userData.youtube_accounts;
    $scope.youtubeAccount = Publish.options.youtubeAccount;
    $scope.channel = true;

    $(document).unbind('youtube');
    $(document).unbind('retrieve');
    $(document).unbind('loginComplete');
    $(document).unbind('loginError');

    $scope.unauthorized = false;
    $scope.channel = true;
    $scope.privacy = [
        {id: 0, text: 'Public'},
        {id: 1, text: 'Unlisted'},
        {id: 2, text: 'Private'}
    ];

    $scope.userData = Publish.userData;
    $scope.form = Publish.userData.form;

    $scope.$parent.validate = function () {
        var len = Publish.userData.youtube_accounts.length;
        if (Publish.options.youtubeAccount && len) {
            s.checkUserYoutube({id: Publish.options.youtubeAccount});
            if (typeof Publish.options.youtubeAccount.valid == "undefined") {
                $(document).bind('youtube.ok', function () {
                    $scope.$parent.validated = true;
                });
                $(document).bind('youtube.faild', function () {
                    $scope.$parent.validated = false;
                    $scope.channel = false;
                });
                $(document).bind('youtube.unauthorized', function () {
                    $scope.$parent.validated = false;
                    $scope.unauthorized = true;
                });
            }
            else if (Publish.options.youtubeAccount.valid) {
                //$location.path($scope.$parent.routes.step_export);
            }
        } else {
            $scope.notSelected = true;
            $scope.$parent.validated = false;
        }

        return false;
    };

    $(document).bind('retrieve.youtube', function () {
        $scope.youtubeAccount = Publish.options.youtubeAccount;
        $scope.youtubeAccounts = Publish.userData.youtube_accounts;
    });

    $scope.addAccount = function () {
        $(window)[0].open("/account/login/google-oauth2/", 'Login', 'width=500,height=700');
        $(document).bind('loginComplete', function () {

            s.getYoutubeAccountDetails();
        });
    };

    $scope.removeAccount = function (account) {
        $('.preloader').fadeIn();
        s.deleteYoutubeAccount(account).then(function () {
            if ($scope.youtubeAccounts.length && account.id === $scope.youtubeAccount) {
                $scope.youtubeAccount = $scope.youtubeAccounts[0].id;
                Publish.options.youtubeAccount = $scope.youtubeAccounts[0].id;
            } else if ($scope.youtubeAccounts.length === 0) {
                $scope.youtubeAccount = false;
                Publish.options.youtubeAccount = false;
            }
        });

        $scope.channel = true;
    };

    $scope.setYoutubeAccount = function (id) {
        $scope.youtubeAccount = id;
        s.setYoutubeAccount(id);
    }
};
