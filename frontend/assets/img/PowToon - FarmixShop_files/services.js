angular.module('publishModalApp')
    .factory('mainService', MainService);

MainService.$inject = ['$http', '$location', 'Publish'];

function MainService($http, $location, Publish) {
    var urls;

    var settings = {isMoreOptionsCollapsed: false};
    var errors = [];
    var videoCategories = [{text: 'Select a category', id: 0}];
    var videoRestrictions = [
        {id: 0, text: "Public"},
        {id: 1, text: "Password Protected (Coming soon)", disabled: true, premium: true, comingSoon: true},
        {id: 2, text: "Unlisted", premium: true, disabled: true}];

    var exportQualities = [
        {
            name: 'standard',
            text: "Standard",
            quality: "480",
            icons: ['mobile', 'pc'],
            strip: '',
            disabled: false
        },
        {
            name: 'hd',
            text: "HD Quality",
            quality: "720",
            icons: ['mobile', 'pc', 'projector', 'tablet'],
            strip: "pro",
            disabled: false
        },
        {
            name: 'full_hd',
            text: "Full HD",
            quality: "1080",
            icons: ['mobile', 'pc', 'projector', 'tablet', 'blu-ray', 'tv'],
            strip: "business",
            disabled: false
        }
    ];
    var beingExported = false;
    var wistiaUrl = "";

    var model = {
        settings: settings,
        beingExported: beingExported,
        mode: Publish.mode,
        videoCategories: videoCategories,
        videoRestrictions: videoRestrictions,
        exportOptions: Publish.options,
        exportQualities: exportQualities,
        userData: Publish.userData,
        getUserPlans: function () {
            var $this = this;

            $http.get('/account/licensing/json/' + Publish.powtoon + "/")
                .success(function (data) {

                    if (data.watermark == false) {
                        Publish.options.watermark = data.watermark;
                    }

                    Publish.userData.canHideWatermark = !Publish.options.watermark;
                    Publish.userData.plan = data;
                    Publish.userData.isPremium = Publish.userData.plan.quality > 480;
                    Publish.userData.form.title = data.title;
                    exportQualities[1].disabled = Publish.userData.plan.quality < 720;
                    exportQualities[2].disabled = Publish.userData.plan.quality < 1080;

                    if(!exportQualities[1].disabled) {
                        exportQualities[1].strip = ''
                    }

                    if(!exportQualities[2].disabled) {
                        exportQualities[2].strip = ''
                    }

                    if (Publish.userData.isPremium) {
                        $this.videoRestrictions.forEach(function(restriction) {
                            if (restriction.premium) {
                                restriction.disabled = false;
                                restriction.premium = false;
                            }
                        });

                        Publish.userData.form.privacy = {id: 2, text: 'Unlisted'};
                    }

                    model.mode = data.mode;

                    if (Publish.userData.plan.indexed) {
                        $this.setUnlist({id: 0, text: "Public"});
                    } else {
                        $this.setUnlist({id: 2, text: "Unlisted"});
                    }
                    $this.getFacebookAccountDetails();
                    $this.getYoutubeAccountDetails();
                    $this.getThumbUrl();
                    $this.getWistiaAccountDetails();
                    $this.getVimeoAccountDetails();
                    $this.getHubspotAccountDetails();

                }).error(function (data, status) {
                console.error(data.error);
                errors.push({
                    status: status,
                    message: "Could'nt get user's plan from server for powtoon " + Publish.userData.flashData.p
                });
                $location.path("Error");
            });
        },
        checkUserYoutube: function (account) {
            $http.get(urls.check_user_youtube + '?id=' + account.id, {
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
            }).success(function (data) {
                account['valid'] = true;
                if (data.has_chanel && data.has_chanel == true) {
                    $(document).trigger('youtube.ok');
                } else if (data.unauthorized) {
                    $(document).trigger('youtube.unauthorized');
                } else {
                    $(document).trigger('youtube.faild');
                }
            }).error(function (data, status) {
                errors.push({status: status, message: "Check YouTube channel failed, close the modal and try again "});
                $location.path("Error");
            });
        },
        checkUserFacebook: function (account) {
            $http.get(urls.check_user_facebook + '?id=' + account.id, {
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
            }).success(function (data) {
                account['valid'] = true;
                if (data.has_chanel && data.has_chanel == true) {
                    $(document).trigger('facebook.ok');
                } else if (data.unauthorized) {
                    $(document).trigger('facebook.unauthorized');
                } else {
                    $(document).trigger('facebook.faild');
                }
            }).error(function (data, status) {
                errors.push({status: status, message: "Check Facebook channel faild, close the modal and try again "});
                $location.path("Error");
            });
        },
        addYoutubeAccount: function (account) {
            Publish.userData.youtube_accounts.push(account);
        },
        addFacebookAccount: function (account) {
            Publish.userData.facebook_accounts.push(account);
        },
        getThumbUrl: function () {
            $http.get("/presentoons/" + Publish.powtoon + "/thumb/").success(function (data) {
                if (data.thumb_url.length) {
                    Publish.userData.thumb = data.thumb_url;
                }
                else {
                    Publish.userData.thumb = "//powtoon.s3.amazonaws.com/images/website/publish/pt.png";
                }
            }).error(function (data, status) {
                Publish.userData.thumb = "//powtoon.s3.amazonaws.com/images/website/publish/pt.png";
            });
        },
        getVideoCategories: function () {
            if (videoCategories.length < 2) {
                $http.get(urls.get_categories).success(function (data) {
                    $.each(data.categories, function (item, val) {
                        videoCategories.push({id: val.id, text: val.name});
                    });

                    return;
                }).error(function (data, status) {
                    errors.push({status: status, message: "Could'nt get video categories "});
                    $location.path("Error");
                });
            }
        },
        getYoutubeAccountDetails: function () {

            $http.get(urls.get_user_youtube).success(function (data) {

                $.each(data, function (item, value) {
                    if (!$.grep(Publish.userData.youtube_accounts, function (e) {
                            return e.id == value.id;
                        }).length) {
                        Publish.userData.youtube_accounts.push(value);
                    }
                });
                if (Publish.userData.youtube_accounts.length) {
                    Publish.options.youtubeAccount = Publish.userData.youtube_accounts[0].id;
                    $(document).trigger('retrieve.youtube');
                }

            }).error(function (data, status) {
                var email = Publish.userData.ut && Publish.userData.ut.email ? Publish.userData.ut.email : "";
                errors.push({status: status, message: "Could'nt get user youtube account details for user " + email});
                $location.path("Error");
            });
        },
        getFacebookAccountDetails: function () {
            $http.get(urls.get_user_facebook).success(function (data) {

                if (data.url && !Publish.userData.facebook_url.length) {
                    Publish.userData.facebook_url = data.url;
                }

                $.each(data, function (item, value) {
                    if (!$.grep(Publish.userData.facebook_accounts, function (e) {
                            return e.id == value.id;
                        }).length) {
                        Publish.userData.facebook_accounts.push(value);
                    }
                });
                if (Publish.userData.facebook_accounts.length) {
                    Publish.options.facebookAccount = Publish.userData.facebook_accounts[0].id;
                    $(document).trigger('retrieve.facebook');
                }

            }).error(function (data, status) {
                var email = Publish.userData.ut && Publish.userData.ut.email ? Publish.userData.ut.email : "";
                errors.push({status: status, message: "Could'nt get user facebook account details for user " + email});
                $location.path("Error");
            });
        },
        getWistiaAccountDetails: function () {
            $http.get(urls.get_wistia_account).success(function (data) {

                if (data.url && !Publish.userData.wistia_url.length) {
                    Publish.userData.wistia_url = data.url;
                }
                if (data.uid && data.id) {
                    if (!Publish.userData.wistia_accounts.length) {
                        Publish.userData.wistia_accounts.push(data);
                        Publish.options.wistiaAccount = Publish.userData.wistia_accounts[0].id;
                        $(document).trigger('retrieve.wistia');
                    }
                }
            }).error(function (data, status) {
                var email = Publish.userData.ut && Publish.userData.ut.email ? Publish.userData.ut.email : "";
                errors.push({status: status, message: "Could'nt get wistia account details for user " + email});
                $location.path("Error");
            });
            return wistiaUrl; // WTF??? it always returns an empty string!!!
        },
        getVimeoAccountDetails: function () {
            $http.get(urls.get_vimeo_account).success(function (data) {

                if (data.url && !Publish.userData.vimeo_url.length) {
                    Publish.userData.vimeo_url = data.url;
                }
                if (data.uid && data.id) {
                    if (!Publish.userData.vimeo_accounts.length) {
                        Publish.userData.vimeo_accounts.push(data);
                        Publish.options.vimeoAccount = Publish.userData.vimeo_accounts[0].id;
                        $(document).trigger('retrieve.vimeo');
                    }
                }
            }).error(function (data, status) {
                var email = Publish.userData.ut && Publish.userData.ut.email ? Publish.userData.ut.email : "";
                errors.push({status: status, message: "Couldn't get vimeo account details for user " + email});
                $location.path("Error");
            });
        },
        getHubspotAccountDetails: function () {
            $http.get(urls.get_hubspot_account).success(function (data) {

                if (data.url && !Publish.userData.hubspot_url.length) {
                    Publish.userData.hubspot_url = data.url;
                }
                if (data.uid && data.id) {
                    if (!Publish.userData.hubspot_accounts.length) {
                        Publish.userData.hubspot_accounts.push(data);
                        Publish.options.hubspotAccount = Publish.userData.hubspot_accounts[0].id;
                        $(document).trigger('retrieve.hubspot');
                    }
                }
            }).error(function (data, status) {
                var email = Publish.userData.ut && Publish.userData.ut.email ? Publish.userData.ut.email : "";
                errors.push({status: status, message: "Couldn't get hubspot account details for user " + email});
                $location.path("Error");
            });
        },
        deleteYoutubeAccount: function (account) {
            var post_data = {id: account.id};
            return $http.get(urls.delete_social_auth_account + '?id=' + account.id).then(function () {
                $('.preloader').fadeOut();
                var position = Publish.userData.youtube_accounts.map(function(account) {return account.id;}).indexOf(account.id);

                if (position !== -1) {
                    Publish.userData.youtube_accounts.splice(position, 1);
                }
            }, function (data, status) {
                var email = Publish.userData.ut && Publish.userData.ut.email ? Publish.userData.ut.email : "";
                errors.push({status: status, message: "Error removing youtube account for " + email});
            });
        },

        deleteFacebookAccount: function (account) {
            var post_data = {id: account.id};
            return $http.get(urls.delete_social_auth_account + '?id=' + account.id).then(function () {
                $('.preloader').fadeOut();
                var position = Publish.userData.facebook_accounts.map(function(account) {return account.id;}).indexOf(account.id);

                if (position !== -1) {
                    Publish.userData.facebook_accounts.splice(position, 1);
                }
            }, function (data, status) {
                var email = Publish.userData.ut && Publish.userData.ut.email ? Publish.userData.ut.email : "";
                errors.push({status: status, message: "Error removing facebook account for " + email});
            });
        },
        deleteWistiaAccount: function (account) {
            var post_data = {id: account.id};

            return $http.get(urls.delete_social_auth_account + '?id=' + account.id).then(function () {
                $('.preloader').fadeOut();
                var position = Publish.userData.wistia_accounts.map(function(account) {return account.id;}).indexOf(account.id);
                if (~position) Publish.userData.wistia_accounts.splice(position, 1);

            }, function (data, status) {
                var email = Publish.userData.ut && Publish.userData.ut.email ? Publish.userData.ut.email : "";
                errors.push({status: status, message: "Error removing wistia account for " + email});
                $location.path("Error");
            });
        },
        deleteVimeoAccount: function (account) {
            var post_data = {id: account.id};

            return $http.get(urls.delete_social_auth_account + '?id=' + account.id).then(function () {
                $('.preloader').fadeOut();
                var position = Publish.userData.vimeo_accounts.map(function(account) {return account.id;}).indexOf(account.id);
                if (~position) Publish.userData.vimeo_accounts.splice(position, 1);

            }, function (data, status) {
                var email = Publish.userData.ut && Publish.userData.ut.email ? Publish.userData.ut.email : "";
                errors.push({status: status, message: "Error removing vimeo account for " + email});
                $location.path("Error");
            });
        },
        deleteHubspotAccount: function (account) {
            var post_data = {id: account.id};

            return $http.get(urls.delete_social_auth_account + '?id=' + account.id).then(function () {
                $('.preloader').fadeOut();
                var position = Publish.userData.hubspot_accounts.map(function(account) {return account.id;}).indexOf(account.id);
                if (~position) Publish.userData.hubspot_accounts.splice(position, 1);

            }, function (data, status) {
                var email = Publish.userData.ut && Publish.userData.ut.email ? Publish.userData.ut.email : "";
                errors.push({status: status, message: "Error removing hubspot account for " + email});
                $location.path("Error");
            });
        },
        setCollapse: function (collapse) {
            angular.extend(settings, {isMoreOptionsCollapsed: collapse});
        },
        setUploadVendor: function (val) {
            Publish.options.uploadTo = val;
            if (val == 'mp4') {
                if (Publish.userData.plan.download === false) { // TODO: figure out what is this for
                    Publish.options.uploadTo = false;
                }
            }
        },
        setYoutubeAccount: function (val) {
            Publish.options.youtubeAccount = val;

        },
        setFacebookAccount: function (val) {
            Publish.options.facebookAccount = val;

        },
        setVimeoAccount: function (val) {
            Publish.options.vimeoAccount = val;

        },
        setHubspotAccount: function (val) {
            Publish.options.hubspotAccount = val;

        },
        setWistiaAccount: function (val) {
            Publish.options.wistiaAccount = val;

        },
        validateSlideshareAccount: function (username, password, callback) {

            $http.post(urls.check_slide_share_credentials, {
                    slideshare_login: username,
                    slideshare_password: password
                })
                .success(function check_slide_share_credentials_success(data) {
                    Publish.options.slideshareAccount.username = username;
                    Publish.options.slideshareAccount.password = password;
                    try {
                        callback((data.success) ? 'accepted' : 'rejected'); // invoke callback if supplied
                    }
                    catch (err) {/**/
                    }
                })
                .error(function check_slide_share_credentials_error(data, status) {
                    callback('unexpected error');
                });
        },
        setUnlist: function (val) {
            Publish.options.unlist = val;

        },
        setWatermark: function (val) {
            Publish.options.watermark = val;
        },
        setExportQuality: function (val) {
            Publish.options.quality = val;

        },
        deleteCurrentJob: function (powtoon) {
            $http.post(urls.delete_current_job);
        },
        isBeingPublished: function () {
            var $this = this;
            $http.get(urls.is_being_published).success(function (data) {
                    if (data.conversions > 0) {
                        $location.path('/BeingPublished');
                    }
                    else {
                        $this.getUserPlans();
                    }
                })
                .error(function (data, status) {
                    errors.push({status: status, message: "Error retrieving information on your powtoon "});
                    $location.path("Error");
                    //$location.path('/BeingPublished');
                });

        },
        errors: errors,
        updateFormDetails: function (form) {
            Publish.options.form = form;
        },
        init: function (vendor) {
            urls = {
                get_user_youtube: '/account/get_user_youtube/',
                check_user_youtube: '/account/check_user_youtube/',
                get_user_facebook: '/account/get_user_facebook/',
                check_user_facebook: '/account/check_user_facebook/',
                get_lic_info: '/account/licensing/json/' + Publish.powtoon + "/",
                get_categories: '/presentoons/ajax/project-categories/',
                delete_social_auth_account: '/account/linked-accounts/delete_ajax/',
                convert2video: "/presentoons/publish-video/",
                get_thumb_url: "/presentoons/" + Publish.powtoon + "/thumb/",
                get_wistia_account: "/account/get-connected-account/wistia/",
                get_vimeo_account: "/account/get-connected-account/vimeo/",
                get_hubspot_account: "/account/get-connected-account/hubspot/",
                is_being_published: "/presentoons/converting/" + Publish.powtoon + "/",
                delete_current_job: "/presentoons/conversion/delete/" + Publish.powtoon + "/",
                check_slide_share_credentials: "/export/check-slide-share-credentials/"
            };
            this.getVideoCategories();
            this.deleteCurrentJob();
            this.getUserPlans();

            if (vendor) {
                this.setUploadVendor(vendor);
                this.setCollapse(false);
            }
            else {
                this.setCollapse(true);
            }
        }
    };

    return model;
}
