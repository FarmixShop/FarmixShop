angular.module('publishModalApp')
    .controller('WistiaController', WistiaController);

WistiaController.$inject = ['$scope', '$location', 'mainService', 'Publish'];
function WistiaController($scope, $location, s, Publish) {
    $scope.wistiaAccounts = Publish.userData.wistia_accounts;
    $scope.wistiaAccount = Publish.options.wistiaAccount;
    $scope.validation = true;
    $scope.wistiaUrl = Publish.userData.wistia_url;

    $(document).unbind('loginComplete');
    $(document).unbind('retrieve');
    $(document).unbind('loginError');

    $scope.$parent.validate = function () {
        $scope.$parent.validated = Publish.options.wistiaAccount && Publish.userData.wistia_accounts.length;
    };

    $scope.addAccount = function () {
        $(window)[0].open(Publish.userData.wistia_url, 'Login', 'width=900,height=700');
        $(document).bind('loginComplete', function () {
            s.getWistiaAccountDetails();
        });
        $(document).bind('retrieve.wistia', function () {
            $scope.wistiaAccount = Publish.options.wistiaAccount;
            $scope.wistiaAccounts = Publish.userData.wistia_accounts;
        });
        $(document).bind('loginError', function (e, data) {
            $scope.loginError = true;
        });
    };

    $(document).bind('retrieve.wistia', function () {
        $scope.wistiaAccount = Publish.options.wistiaAccount;
        $scope.wistiaAccounts = Publish.userData.wistia_accounts;
    });

    $scope.removeAccount = function (account) {
        $('.preloader').fadeIn();
        s.deleteWistiaAccount(account).then(function () {
            if ($scope.wistiaAccounts.length && account.id === $scope.wistiaAccount) {
                $scope.wistiaAccount = $scope.wistiaAccounts[0].id;
                Publish.options.wistiaAccount = $scope.wistiaAccounts[0].id;
            } else if ($scope.wistiaAccounts.length === 0) {
                $scope.wistiaAccount = false;
                Publish.options.wistiaAccount = false;
            }
        });
    };

    $scope.setWistiaAccount = function (id) {
        $scope.wistiaAccount = id;
        s.setWistiaAccount(id);
    }
}