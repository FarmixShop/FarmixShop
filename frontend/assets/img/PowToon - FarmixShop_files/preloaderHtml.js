(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 250,
	height: 200,
	fps: 25,
	color: "#FFFFFF",
	manifest: []
};



// symbols:



(lib._4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F53654").s().p("AgsAtQgUgTABgaQgBgZAUgUQASgSAagBQAaABAUASQASAUABAZQgBAagSATQgUAUgaAAQgaAAgSgUg");
	this.shape.setTransform(17.4,8.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F53654").s().p("AgtAvQgSgSAAgdQAAgbASgSQASgSAbAAQAcAAASARQASASAAAcQgBAdgRASQgSARgcAAQgcAAgRgRg");
	this.shape_1.setTransform(17.4,8.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F53654").s().p("AgtAwQgRgRAAgfQgBgdASgRQARgRAcAAQAfAAAQARQAQAQAAAeQAAAfgRARQgPAQgfAAQgdAAgQgQg");
	this.shape_2.setTransform(17.4,8.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F53654").s().p("AAABAQggAAgPgPQgOgPAAgiQgBgfAQgQQAQgQAeAAQAgAAAQAQQAOAPAAAgQAAAhgPAQQgOAPgfAAIgCAAg");
	this.shape_3.setTransform(17.4,8.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F53654").s().p("AAABAQghgBgOgNQgNgOAAgjQgBgiAPgPQAPgPAgAAQAhABAPAOQANAOAAAiQgBAjgNAPQgNAOghAAIgCAAg");
	this.shape_4.setTransform(17.3,8.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#F53654").s().p("AAABAQgigBgNgNQgNgNAAgkQgBgiAPgPQAPgPAgAAQAiABAOANQANAOAAAjQgBAkgNAOQgNAOghAAIgCAAg");
	this.shape_5.setTransform(17.3,8.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#F53654").s().p("AAABAQgigBgNgNQgNgNAAgkQgBgjAPgOQAPgPAgAAQAiABAOANQANAOgBAjQAAAkgNAOQgMAOghAAIgDAAg");
	this.shape_6.setTransform(17.3,8.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F53654").s().p("AAAA/QgiAAgNgMQgNgOAAgkQgBgjAOgOQAPgPAhAAQAiABAOANQANANgBAkQAAAkgNAPQgMANghAAIgDgBg");
	this.shape_7.setTransform(17.3,8.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#F53654").s().p("AAAA/QgjgBgMgLQgMgNgBglQgBgjAOgPQAPgOAhAAQAjABANANQANANgBAkQgBAlgMAOQgLANgiAAIgDgBg");
	this.shape_8.setTransform(17.3,8.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#F53654").s().p("AAAA/QgjgBgMgLQgMgNgBglQgBgkAOgOQAPgOAhAAQAjABANAMQANANgBAlQgBAlgMAOQgLANgiAAIgDgBg");
	this.shape_9.setTransform(17.3,8.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F53654").s().p("AguAxQgOgOgBgjQAAggAPgQQAQgPAfAAQAhAAAOAPQAPAOgBAiQgBAjgNAOQgOAPgiAAQghgBgNgOg");
	this.shape_10.setTransform(17.3,8.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#F53654").s().p("AAABAQgfgBgPgPQgPgQAAggQgBgeARgRQAQgQAdAAQAgAAAPAQQAQAQAAAfQAAAggQAQQgPAQgeAAIgCAAg");
	this.shape_11.setTransform(17.4,8.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#F53654").s().p("AgtAvQgRgSAAgdQgBgcASgSQASgRAbAAQAdAAARARQASASAAAcQAAAdgSASQgRARgdAAQgcAAgRgRg");
	this.shape_12.setTransform(17.4,8.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},3).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(27));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:true},1).wait(12).to({_off:false},0).wait(5).to({_off:true},1).wait(27));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(10.9,1.9,13,13);


(lib._3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFC400").s().p("AhMBNQggggAAgtQAAgsAgggQAgggAsAAQAtAAAgAgQAgAgAAAsQAAAtggAgQggAggtAAQgsAAggggg");
	this.shape.setTransform(21.2,7.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFC400").s().p("AhOBQQgdgggCgwQgCgwAhgeQAggfAvgBQAygBAdAiQAdAhACAtQABAvggAfQggAfgwABIAAAAQgwAAgegfg");
	this.shape_1.setTransform(21,7.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFC400").s().p("AhQBTQgbgfgEg0QgDgzAhgdQAggeA0gCQA1gBAbAjQAbAiACAvQADAxggAdQgfAeg0ACIgEAAQgyAAgageg");
	this.shape_2.setTransform(20.9,7.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFC400").s().p("AhSBWQgZgfgFg3QgGg4AigbQAhgcA3gDQA5gCAYAkQAYAkAEAwQAEAzggAcQgfAdg2ACIgIABQgyAAgYgdg");
	this.shape_3.setTransform(20.8,7.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFC400").s().p("AhTBaQgYgegGg8QgIg7AigaQAigbA7gDQA9gDAVAlQAWAmAFAyQAFA0gfAbQgfAbg6ADIgNABQgyAAgUgbg");
	this.shape_4.setTransform(20.6,7.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFC400").s().p("AhVBcQgVgdgJg/QgJg/AjgZQAhgZBAgEQBBgEASAnQATAmAHA0QAGA2gfAaQgfAag+AEIgOAAQg0AAgSgag");
	this.shape_5.setTransform(20.4,7.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFC400").s().p("AhVBeQgUgegKhAQgKhAAigZQAigZBBgEQBDgFARAoQARAnAIA0QAHA3gfAZQgeAahAAFIgQABQgzAAgRgag");
	this.shape_6.setTransform(20.4,8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFC400").s().p("AhVBgQgTgegLhCQgMhAAjgZQAjgZBCgFQBFgFAPAoQAQAoAJA1QAIA3gfAZQgfAZhAAGIgVABQgxAAgPgZg");
	this.shape_7.setTransform(20.3,8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFC400").s().p("AhVBhQgSgegNhDQgMhBAjgZQAigZBFgFQBGgGAOApQAOAoAKA2QAJA3gfAZQgeAZhCAHIgXABQgwAAgOgZg");
	this.shape_8.setTransform(20.2,8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFC400").s().p("AhWBjQgQgegOhFQgOhCAjgZQAkgYBGgGQBIgGAMApQAMApALA2QALA4gfAZQgfAYhDAIIgZABQgvAAgOgYg");
	this.shape_9.setTransform(20.1,8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFC400").s().p("AhUBgQgTgfgMhBQgMhAAjgZQAjgZBCgGQBFgFAPAoQAPAoAJA1QAKA2ggAaQgfAZhAAHIgWABQgvAAgPgZg");
	this.shape_10.setTransform(20.3,8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFC400").s().p("AhTBeQgVgfgKg/QgLg8AjgbQAigaBAgFQBBgEARAnQASAmAIA0QAIA1gfAbQggAag9AGIgSAAQgwAAgRgZg");
	this.shape_11.setTransform(20.4,7.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFC400").s().p("AhSBbQgXgfgIg8QgJg6AigbQAigbA8gEQA+gEAUAmQAUAlAHAzQAGA0gfAbQggAbg6AFIgOABQgwAAgUgbg");
	this.shape_12.setTransform(20.6,7.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFC400").s().p("AhRBYQgZgggGg4QgHg3AhgcQAigdA5gDQA6gDAXAlQAWAkAFAxQAGAzggAcQgfAcg4AEIgLABQgwAAgWgcg");
	this.shape_13.setTransform(20.7,7.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFC400").s().p("AhQBVQgagfgFg2QgFg0AhgdQAhgeA1gCQA4gCAZAkQAYAjAEAvQAEAxgfAeQggAdg1ADIgIAAQgwAAgYgdg");
	this.shape_14.setTransform(20.8,7.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFC400").s().p("AhPBSQgcgfgDgzQgEgyAhgdQAhgfAygBQA0gCAbAjQAbAiADAuQADAwggAeQggAfgyABIgFAAQgvAAgbgeg");
	this.shape_15.setTransform(21,7.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFC400").s().p("AhNBQQgegggCgwQgCgvAhgfQAggfAvgBQAxgBAdAiQAeAhABAtQABAugfAfQggAggwABIgCAAQguAAgdgfg");
	this.shape_16.setTransform(21.1,7.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},2).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape}]},1).to({state:[]},12).wait(26));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(10.3,-3.3,21.9,22);


(lib._2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#85C959").s().p("AhNBOQggghAAgtQAAgsAgghQAhggAsAAQAtAAAhAgQAgAhAAAsQAAAtggAhQghAggtAAQgsAAghggg");
	this.shape.setTransform(12.2,12.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#85C959").s().p("AABBxQgtgDgkgfQgjgfAEgwQAEgvAdgiQAegiAwADQAtADAkAeQAiAegCAwQgDAtgfAkQgdAhgpAAIgIAAg");
	this.shape_1.setTransform(12.2,12.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#85C959").s().p("AACB0QgugHgmgcQgmgdAHg0QAHgxAbgkQAbgkAyAGQAwAFAlAdQAlAcgFAxQgFAvgeAoQgZAigoAAIgNgBg");
	this.shape_2.setTransform(12.2,12.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#85C959").s().p("AAEB3QgwgKgpgaQgpgbALg3QAKg0AZgmQAZglAzAIQAyAJAoAaQAoAagJAzQgIAxgbArQgYAiglAAQgIAAgJgBg");
	this.shape_3.setTransform(12.2,12.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#85C959").s().p("AAFB6QgxgNgsgZQgsgZAOg5QAPg4AVgmQAXgoA1ALQA0AMArAYQAqAYgMA1QgKAzgaAuQgVAjgjAAQgKAAgMgCg");
	this.shape_4.setTransform(12.2,12.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#85C959").s().p("AAHB9QgygQgvgXQgvgXASg8QARg7AUgpQATgoA4ANQA2AOAtAXQAsAWgOA3QgNA0gZAyQgTAkgjAAQgLAAgMgDg");
	this.shape_5.setTransform(12.1,12.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#85C959").s().p("AAIB/QgzgTgygVQgygVAVg/QAWg+AQgqQARgqA6AQQA4ARAvAUQAvAUgRA5QgPA3gYA0QgRAmghAAQgMAAgPgFg");
	this.shape_6.setTransform(12.1,12.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#85C959").s().p("AAICAIhmgoQgygUAWhAQAWg/AQgrQAQgrA7ASQA3ARAxATQAwATgSA6QgQA4gYA1QgRAmghAAQgNAAgOgFg");
	this.shape_7.setTransform(12.2,12.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#85C959").s().p("AAICBIhngoQgygTAWhBIAnhrQAQgsA6ASQA4ASAxATQAxASgSA6QgRA4gYA3QgRAmghAAQgMAAgPgFg");
	this.shape_8.setTransform(12.2,12.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#85C959").s().p("AAHCCQg0gWgzgSQgzgSAYhDIAnhsQAPgsA6ASIBqAlQAyARgSA7QgSA5gYA3QgRAnghAAQgNAAgPgFg");
	this.shape_9.setTransform(12.3,12.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#85C959").s().p("AAHCCQg0gWgzgSQg0gRAYhDQAZhCAOgtQAPgsA7ATQA4AUAzAQQAyARgSA8QgSA5gYA4QgSApghAAQgNAAgPgHg");
	this.shape_10.setTransform(12.3,12.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#85C959").s().p("AAHCDQg1gXgzgRQg1gQAZhFQAahDAOgtQAOgtA7AVQA4AUA0APQA0AQgUA8QgSA7gYA5QgSApggAAQgNAAgQgHg");
	this.shape_11.setTransform(12.4,12.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#85C959").s().p("AAGCEQg0gYg0gQQg1gQAahFQAahEAOgtQANguA7AVQA5AVA1APQA0AOgUA9QgTA8gYA6QgSAoggAAQgOAAgQgGg");
	this.shape_12.setTransform(12.4,12.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#85C959").s().p("AAGCEQg1gYg0gPQg1gPAahGQAbhFAOguQANguA6AVQA5AWA2AOQA1ANgUA+QgUA9gYA6QgSAqggAAQgOAAgQgIg");
	this.shape_13.setTransform(12.5,12.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#85C959").s().p("AAGCFQg1gZg1gOQg2gOAchIQAbhGANguQANgvA7AXQA5AWA2ANQA2AMgUA/QgVA9gYA8QgSAqggAAQgOAAgQgIg");
	this.shape_14.setTransform(12.5,12.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#85C959").s().p("AAFCCQg0gWgygRQgygRAXhDIAohvQAPgtA5AUQA3ATA0APQAyAQgRA8QgSA7gZA3QgTApghAAQgNAAgPgGg");
	this.shape_15.setTransform(12.5,12.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#85C959").s().p("AAEB+QgygSgwgTQgvgTAUhBQATg+ATgqQASgrA3AQQA1AQAxASQAwATgPA6QgPA4gaA0QgVAmgiAAQgLAAgOgFg");
	this.shape_16.setTransform(12.4,12.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#85C959").s().p("AADB7QgxgOgtgWQgsgWAQg9QAPg6AWgpQAVgoA1ANQAzAMAuAVQAsAWgMA3QgLA2gcAwQgVAlgkAAQgKAAgMgEg");
	this.shape_17.setTransform(12.4,12.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#85C959").s().p("AACB4QgwgLgpgYQgqgZAMg5QAMg3AYgmQAYgnAzAKQAyAKAqAYQApAXgIA1QgJAzgdAtQgYAjglAAQgJAAgJgCg");
	this.shape_18.setTransform(12.4,12.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#85C959").s().p("AABB1QgugIgngbQgmgcAIg0QAIg0AagkQAbglAyAHQAvAGAnAbQAmAbgFAyQgGAxgeAoQgaAjgnAAIgOgBg");
	this.shape_19.setTransform(12.3,12.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#85C959").s().p("AAABxQgtgDgkgeQgjgeAEgyQAEgvAdgiQAfgjAvADQAtAEAkAdQAkAegDAwQgDAugfAlQgdAhgqAAIgIgBg");
	this.shape_20.setTransform(12.3,12.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape}]},1).to({state:[]},14).wait(14));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1.2,1.2,22.1,22.2);


(lib._1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3A64CE").s().p("AhVBWQgjgkgBgyQABgxAjgkQAkgkAxAAQAyAAAkAkQAjAkAAAxQAAAygjAkQgkAkgyAAQgxAAgkgkg");
	this.shape.setTransform(12.2,12.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3A64CE").s().p("AABB7QgzgCgmgiQgkgiACg0QACgyAhgmQAigmA1ADQAzACAmAiQAlAigDA0QgCAzghAlQggAjgxAAIgGAAg");
	this.shape_1.setTransform(12.4,12.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3A64CE").s().p("AACB9Qg2gFgmggQgmggAEg2QAEgzAfgoQAggoA4AFQA1AEAnAiQAmAggEA2QgEA0ggAnQgcAjguAAIgNgBg");
	this.shape_2.setTransform(12.6,11.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3A64CE").s().p("AADB+Qg4gIgogdQgngeAGg3QAGg1AegqQAdgqA7AHQA3AHApAgQAoAfgHA4QgGA1geApQgZAigtAAIgSgCg");
	this.shape_3.setTransform(12.7,11.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3A64CE").s().p("AAECAQg6gLgpgcQgpgcAIg4QAIg2AcgsQAbgsA9AJQA6AJAqAeQApAegIA6QgJA3gbAqQgXAigrAAQgLAAgMgCg");
	this.shape_4.setTransform(12.9,11.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3A64CE").s().p("AAFCCQg9gOgqgaQgqgaAKg5QAKg4AagtQAaguA/ALQA9ALArAdQArAcgLA7QgLA6gaArQgUAigqAAQgMAAgPgDg");
	this.shape_5.setTransform(13.1,11.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#3A64CE").s().p("AAGCDQg/gQgsgYQgrgYAMg7QAMg5AYgwQAYgwBBAOQBAANAsAcQAsAagMA9QgNA7gYAtQgSAigpAAQgOAAgRgEg");
	this.shape_6.setTransform(13.3,11.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3A64CE").s().p("AAHCEQhCgTgsgVQgtgWAOg9QAOg6AWgyQAVgyBFAQQBBAQAuAaQAuAZgPA/QgPA9gWAuQgQAignAAQgPAAgUgGg");
	this.shape_7.setTransform(13.5,11.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#3A64CE").s().p("AAICGQhEgWgugTQgugUARg+QAQg8ATg0QAUg0BHASQBEASAvAZQAvAXgRBBQgRA/gUAwQgOAhgmAAQgRAAgWgGg");
	this.shape_8.setTransform(13.6,11.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#3A64CE").s().p("AAJCGQhFgWgugTQgvgTARg/QARg8ATg0QATg1BHATQBFATAwAYQAvAXgRBBQgSA/gTAwQgOAigmAAQgRAAgWgHg");
	this.shape_9.setTransform(13.7,11);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#3A64CE").s().p("AAKCGQhHgXgugSQgvgTASg/QARg8ASg1QATg1BIATQBFATAwAYQAwAXgSBBQgSBAgTAxQgNAigmAAQgRAAgWgIg");
	this.shape_10.setTransform(13.7,11);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#3A64CE").s().p("AAKCHQhHgYgvgSQgvgSASg/QASg9ASg1QASg2BIAUQBGAUAxAXQAwAWgSBCQgTBAgSAxQgNAiglAAQgSAAgXgHg");
	this.shape_11.setTransform(13.8,11);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#3A64CE").s().p("AAKCHQhHgYgwgRQgvgSAThAQASg9ARg2QASg2BJAVQBHAUAxAXQAwAWgTBCQgTBAgRAyQgMAigmAAQgSAAgYgIg");
	this.shape_12.setTransform(13.8,10.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#3A64CE").s().p("AALCIQhJgZgvgRQgwgRAThAIAkh0QARg3BKAVQBHAVAxAXQAxAVgUBDQgTBBgRAyQgLAigmAAQgSAAgYgIg");
	this.shape_13.setTransform(13.9,10.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#3A64CE").s().p("AALCIIh5gqQgwgRAUhAIAjh1QARg3BKAWQBIAVAyAWQAxAVgUBDIgkB0QgLAiglAAQgTAAgZgIg");
	this.shape_14.setTransform(13.9,10.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#3A64CE").s().p("AAMCIIh7gqQgwgQAUhBIAkh1QAQg4BLAWQBIAWAyAWQAzAVgWBDIgkB1QgJAigmAAQgSAAgagJg");
	this.shape_15.setTransform(14,10.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#3A64CE").s().p("AALCIQhJgZgvgRQgwgRAThAIAkh0QARg3BKAVQBHAVAxAWQAxAWgUBDIgkBzQgKAigmAAQgSAAgZgIg");
	this.shape_16.setTransform(13.9,10.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#3A64CE").s().p("AALCHQhIgYgvgSQgvgSASg/QASg9ASg1QASg2BIAUQBGAUAxAXQAxAWgTBCQgTBAgRAxQgMAigmAAQgSAAgXgHg");
	this.shape_17.setTransform(13.8,11);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#3A64CE").s().p("AAKCGQhHgWgugTQgugTARg/QARg8ASg0QATg1BIATQBFATAwAXQAvAYgRBBQgSA+gSAxQgNAignAAQgRAAgWgHg");
	this.shape_18.setTransform(13.7,11);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#3A64CE").s().p("AAJCGQhFgVgugUQgtgUAQg+QAQg8ATgzQAUg0BHASQBDARAvAZQAvAYgQBAQgRA+gTAwQgOAignAAQgRAAgVgGg");
	this.shape_19.setTransform(13.6,11.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#3A64CE").s().p("AAJCFQhEgUgtgVQgtgVAPg9QAPg7AUgyQAVgzBFARQBDAQAuAZQAvAZgQA/QgQA9gUAvQgOAigoAAQgQAAgUgFg");
	this.shape_20.setTransform(13.5,11.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#3A64CE").s().p("AAICEQhCgSgtgWQgsgWAOg9QAOg6AVgxQAWgyBEAQQBBAPAuAaQAuAZgPA/QgPA8gVAuQgPAigpAAQgPAAgTgFg");
	this.shape_21.setTransform(13.4,11.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#3A64CE").s().p("AAGCCQg+gOgqgaQgrgZALg6QAKg4AZguQAaguBAAMQA8ALAsAcQArAcgLA8QgLA5gZAsQgTAigrAAQgMAAgPgDg");
	this.shape_22.setTransform(13.1,11.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#3A64CE").s().p("AAEB/Qg6gJgogdQgogdAHg4QAHg1AdgqQAdgrA7AIQA5AHApAfQAoAfgHA4QgIA2gcAqQgYAigtAAIgTgCg");
	this.shape_23.setTransform(12.8,11.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#3A64CE").s().p("AACB8Qg1gEgnghQglggADg1QAEgzAggnQAggnA3ADQA1AEAmAiQAmAhgDA1QgEA0ggAmQgdAjgvAAIgLgBg");
	this.shape_24.setTransform(12.5,12);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},2).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape}]},1).to({state:[]},20).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,24.4,24.5);


(lib.all = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AgzAzQgVgVAAgeQAAgdAVgVQAWgWAdAAQAeAAAWAWQAVAVAAAdQAAAegVAVQgWAWgeAAQgdAAgWgWg");
	mask.setTransform(9,7.4);

	// 1
	this.instance = new lib._1("synched",0);
	this.instance.setTransform(10.3,7.2,0.188,0.187,0.3,0,0,4.1,19.6);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:4,regY:19.8,scaleX:0.21,scaleY:0.21,rotation:24.1,x:10.2,startPosition:7},7).to({regX:4.3,regY:19.6,scaleX:0.34,scaleY:0.33,rotation:170.8,x:9.9,y:7.6,startPosition:13},6,cjs.Ease.get(0.05)).to({regX:4.4,scaleX:0.31,scaleY:0.31,rotation:240.1,y:7.4,startPosition:18},5).to({regX:4,regY:19.7,scaleX:0.24,scaleY:0.24,rotation:281.5,y:7.3,startPosition:21},3,cjs.Ease.get(1)).to({regY:19.6,scaleX:0.19,scaleY:0.19,rotation:343.6,x:10.3,y:7.2,startPosition:30},9).to({regX:4.1,rotation:360.3,startPosition:35},5).wait(12));

	// 1
	this.instance_1 = new lib._2("synched",0);
	this.instance_1.setTransform(10.3,7.2,0.058,0.058,-37.7,0,0,4.4,19.9);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5).to({_off:false},0).to({regX:3.9,regY:19.8,scaleX:0.11,scaleY:0.11,rotation:0,skewX:19.7,skewY:24.1,x:10.2,startPosition:7},7,cjs.Ease.get(-0.33)).to({regX:4.3,regY:20.3,scaleX:0.24,scaleY:0.24,skewX:85.7,skewY:85.4,x:9.9,y:7.5,startPosition:12},5).to({regX:4.4,scaleX:0.24,scaleY:0.24,skewX:108.1,skewY:107.8,y:7.4,startPosition:13},1).to({regY:20.4,scaleX:0.25,scaleY:0.25,rotation:130.6,skewX:0,skewY:0,x:9.8,startPosition:14},1).to({regY:20.3,scaleX:0.25,scaleY:0.25,rotation:153.2,startPosition:15},1).to({regY:20.4,scaleX:0.25,scaleY:0.25,rotation:175.7,x:9.9,y:7.3,startPosition:16},1).to({regX:4.2,regY:20.2,scaleX:0.26,scaleY:0.26,rotation:198.2,startPosition:17},1).to({regX:4.4,regY:20.3,scaleX:0.24,scaleY:0.24,rotation:214.3,x:10,y:7.2,startPosition:18},1).to({regX:4.3,regY:20.2,scaleX:0.22,scaleY:0.22,rotation:230.4,x:10.1,startPosition:19},1).to({regY:20.3,scaleX:0.2,scaleY:0.2,rotation:246.7,x:10.2,y:7.1,startPosition:20},1).to({regX:4.7,regY:19.4,scaleX:0.18,scaleY:0.18,rotation:262.7,y:7.2,startPosition:21},1).to({regX:4.5,regY:19.3,scaleX:0.18,scaleY:0.18,rotation:274.5,startPosition:22},1).to({regY:19.5,scaleX:0.18,scaleY:0.18,rotation:286.3,x:10.3,startPosition:23},1).to({regX:4.6,regY:19.1,scaleX:0.17,scaleY:0.17,rotation:298.3,startPosition:24},1).to({regX:4.7,regY:19.4,scaleX:0.17,scaleY:0.17,rotation:310.1,startPosition:25},1).to({regX:4.1,scaleX:0.17,scaleY:0.16,rotation:322},1).to({regX:4.2,scaleX:0.16,scaleY:0.16,rotation:334.9,x:10.4,startPosition:26},1).to({regX:4,regY:19.6,scaleX:0.16,scaleY:0.16,rotation:347.7,x:10.3,y:7.1,startPosition:27},1).to({regX:4.2,regY:19.4,scaleX:0.16,scaleY:0.16,rotation:360.3,y:7.2,startPosition:28},1).wait(13));

	// 1
	this.instance_2 = new lib._3("synched",0);
	this.instance_2.setTransform(10.3,7.2,0.037,0.037,-57.9,0,0,5,20.8);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(16).to({_off:false},0).to({regX:3.4,regY:21.7,scaleX:0.12,scaleY:0.12,rotation:57.6,x:10.2,y:7.3,startPosition:5},5,cjs.Ease.get(-0.33)).to({regX:3.3,scaleX:0.13,scaleY:0.13,rotation:73,x:10.1,startPosition:6},1).to({regX:3.5,regY:21.9,scaleX:0.14,scaleY:0.14,rotation:88.2,y:7.4,startPosition:7},1).to({regX:3.4,regY:21.4,scaleX:0.15,scaleY:0.15,rotation:103.3,startPosition:8},1).to({regX:3.2,regY:21.9,scaleX:0.17,scaleY:0.16,rotation:118.6,x:10,startPosition:9},1).to({regX:3.4,scaleX:0.18,scaleY:0.17,rotation:133.9,startPosition:10},1).to({regX:3.1,regY:21.8,scaleX:0.19,scaleY:0.18,rotation:149.1,startPosition:11},1).to({regX:3.6,regY:19.9,scaleX:0.2,scaleY:0.2,rotation:164.3,y:7.5,startPosition:12},1).to({scaleX:0.19,scaleY:0.19,rotation:0,skewX:173.7,skewY:174.2,y:7.4,startPosition:13},1).to({regX:3.7,regY:19.8,scaleX:0.18,scaleY:0.18,skewX:182.8,skewY:184,y:7.5,startPosition:14},1).to({regX:3.8,regY:20.1,scaleX:0.18,scaleY:0.18,skewX:192.3,skewY:194,y:7.4,startPosition:15},1).to({regX:3.6,regY:19.4,scaleX:0.17,scaleY:0.17,skewX:201.6,skewY:204.1,startPosition:16},1).to({regX:3.5,scaleX:0.17,scaleY:0.17,skewX:212.4,skewY:215.3,startPosition:17},1).to({regX:3.8,scaleX:0.16,scaleY:0.16,skewX:223.4,skewY:226.6,y:7.3,startPosition:18},1).to({regX:3.6,regY:19.3,scaleX:0.16,scaleY:0.16,skewX:234.2,skewY:237.9,x:10.1,startPosition:19},1).to({regX:3.9,scaleX:0.16,scaleY:0.16,skewX:245.2,skewY:249.4,x:10,startPosition:20},1).to({regX:3.6,regY:19.6,scaleX:0.16,scaleY:0.16,skewX:256,skewY:260.7,x:10.1,startPosition:21},1).to({regX:3.2,regY:19.9,scaleX:0.15,scaleY:0.15,skewX:267,skewY:271.8,x:10.2,startPosition:22},1).to({regX:3.7,regY:19.7,scaleX:0.15,scaleY:0.15,skewX:277.6,skewY:283.1,startPosition:23},1).to({regX:3.1,regY:19.4,scaleX:0.15,scaleY:0.15,skewX:288.5,skewY:294.3,startPosition:24},1).to({regX:3.6,scaleX:0.14,scaleY:0.14,skewX:299.3,skewY:305.8,y:7.2,startPosition:25},1).to({regX:4.1,regY:20.2,scaleX:0.14,scaleY:0.14,skewX:310.3,skewY:317.1,startPosition:26},1).to({regX:4,regY:20.7,scaleX:0.14,scaleY:0.14,skewX:331.4,skewY:338.3,startPosition:30},4).wait(1));

	// 1
	this.instance_3 = new lib._4("synched",0);
	this.instance_3.setTransform(10,7.3,0.128,0.127,0,-35.2,-35.1,3.3,19.2);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(21).to({_off:false},0).to({regX:3,regY:19.1,scaleX:0.15,scaleY:0.15,skewX:-19.5,skewY:-19.3,y:7.4,startPosition:1},1).to({regX:3.2,scaleX:0.17,scaleY:0.17,skewX:-3.8,skewY:-3.5,startPosition:2},1).to({regY:19.3,scaleX:0.19,scaleY:0.19,skewX:11.8,skewY:12,y:7.5,startPosition:3},1).to({regX:2.9,regY:19.2,scaleX:0.21,scaleY:0.21,skewX:27.5,skewY:27.7,startPosition:4},1).to({scaleX:0.22,scaleY:0.21,rotation:62.4,skewX:0,skewY:0,x:9.9,startPosition:5},1).to({regX:2.8,scaleX:0.22,scaleY:0.22,rotation:97,y:7.6,startPosition:6},1).to({regX:4,regY:18.9,scaleX:0.23,scaleY:0.23,rotation:131.8,startPosition:7},1).to({regX:3.8,scaleX:0.23,scaleY:0.22,rotation:156,x:10,startPosition:8},1).to({regX:4.1,scaleX:0.22,scaleY:0.22,rotation:180,x:9.9,y:7.5,startPosition:9},1).to({regX:3.8,regY:18.8,scaleX:0.21,scaleY:0.21,rotation:204.3,startPosition:10},1).to({regX:3.9,regY:19,scaleX:0.21,scaleY:0.21,rotation:228.6,startPosition:11},1).to({regX:4.2,scaleX:0.2,scaleY:0.2,rotation:251.7,x:9.8,y:7.4,startPosition:12},1).to({regX:4.1,regY:18.7,scaleX:0.2,scaleY:0.2,rotation:274.8,x:9.9,y:7.3,startPosition:13},1).to({regX:4,regY:19.6,scaleX:0.19,scaleY:0.19,rotation:297.8},1).to({regX:4.2,scaleX:0.19,scaleY:0.18,rotation:306.8,x:10,startPosition:14},1).to({regX:3.8,regY:19.7,scaleX:0.18,scaleY:0.18,rotation:315.8,x:10.1,startPosition:15},1).to({regX:3.9,regY:19.4,scaleX:0.18,scaleY:0.17,rotation:324.7,x:10.2,y:7.2,startPosition:16},1).to({regX:3.5,regY:19.6,scaleX:0.17,scaleY:0.17,rotation:333.7,startPosition:17},1).to({regX:3.1,regY:19.4,scaleY:0.17,rotation:337.2,startPosition:18},1).to({regX:3.4,regY:19.8,rotation:340.4,startPosition:19},1).to({regX:3.5,regY:19.6,scaleY:0.17,rotation:343.7,startPosition:17},1).wait(5));

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#F53654","rgba(255,255,255,0)"],[0,1],0.5,1.9,-0.5,-1.9).s().p("AgBAQQgEgCgBgEIgBgDIAAgBIgEgHQgCgDABgEQABgFAEgCQADgCAEABQACABAGAIQAFAHAAACIAAAFIgDAFQgCAEgEABIgBAAQgDAAgBgBg");
	this.shape.setTransform(13.5,6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#F53654","rgba(255,255,255,0)"],[0,1],0,1.4,0.4,-2.6).s().p("AAGAPIgGgBQgFgBgCgDQgDgDACgEQABgDAEgHIADgIIAAAAQACAAACADIAEAGQABACABAEIAAALQgBAFgCAAIgBgBg");
	this.shape_1.setTransform(13.9,8.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#F53654","rgba(255,255,255,0)"],[0,1],1.2,4.1,3.7,-2.4).s().p("AAAAXQACgEgCADQgDACgEAAQgEAAgDgEQgDgDABgEQAAgEADgCIALgHQAGgFAFgMIADgKIAEAGQADACgDATQgDARgOAKg");
	this.shape_2.setTransform(13,9.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#F53654","rgba(255,255,255,0)"],[0,1],-4,0.3,3.3,-1.9).s().p("AgQAUQgNgBACgFIABgJQAAgEADgBQADgDALACQAJABAOgFQAOgGgDgIQAFAFgDAIQgCAHgIAGQgHAHgIAEQgEACgJAAIgFAAg");
	this.shape_3.setTransform(11.8,11.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#F53654","rgba(255,255,255,0)"],[0,1],-4.6,1.5,4.1,1.8).s().p("AgVAHIAAAAIAAAAIAAgBIgCgBQAAgBAAAAQAAgBAAAAQAAAAgBgBQAAAAgBgBIgBAAQABgCACgEQACgEADgBQADgBAFABQASAMASgBIAAABIAAAAIAAAAQgCADgFADQgFACgKABIgCAAQgHAAgQgEg");
	this.shape_4.setTransform(10,11.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#F53654","rgba(255,255,255,0)"],[0,1],-2.3,-0.9,1.8,1.7).s().p("AAEAPQgRgDgEgHQgEgFABgBIACgGQAAgEAEgDQADgCAEAAQAJAVATALIgDABIgOgCg");
	this.shape_5.setTransform(8.4,10.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#F53654","rgba(255,255,255,0)"],[0,1],-0.9,-1.6,2.2,1.7).s().p("AAGAUQgFgCgJgJQgKgJAEgFQADgIADgDQADgDAEAAQACgBADADQADADAAAIQgBAGACAHQACAHAEABQABAAABAAQAAABAAAAQAAAAAAABQAAABgBAAQgCACgEAAIgDAAg");
	this.shape_6.setTransform(7,9.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#F53654","rgba(255,255,255,0)"],[0,1],-0.1,-1.6,0.6,4).s().p("AgFAQQgDgDgBgEIAAgIQAAgFACgGQABgEAEgCQACgCAEACQADABACAEQACAEgBAEIgBAEIAAAFQABAEgDAEQgCADgEABIgBAAQgCAAgDgCg");
	this.shape_7.setTransform(6.3,7.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#F53654","rgba(255,255,255,0)"],[0,1],-824.4,-4.6,814,-4.6).s().p("AgFAQQgEgBgCgEQgCgDABgEQADgIAHgIQACgDADAAQAEAAADADQADACAAAEQAAAEgCACQgEAEgBAFQgCAEgDACIgDABIgDAAg");
	this.shape_8.setTransform(6.8,6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},25).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[]},1).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(9.5,3.5,4.6,4.6);


// stage content:



(lib.preloaderHtmlfile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.all();
	this.instance.setTransform(145,75,12.498,12.498,0,0,0,11.6,6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(47));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(145.6,100,183.8,183.7);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;