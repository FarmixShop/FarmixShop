app.directive('selectMe', [function () {
	return {
		restrict: 'A',
		link: function (scope, el, iAttrs) {
			el.triggerHandler('click');
		}
	};
}]);

app.directive('injectCms', ['$http','$timeout',function ($http,$timeout) {
	return {
		restrict: 'A',
		link: function (scope, el, iAttrs) {
      scope.loading_pricing=true;
      scope.loading_pricing_error=false;
			el.parent().parent().addClass('wider');

			$.ajax({
        url:'/pricing/popup',
        success:function(data) {
          el.html(data);
          $timeout(function(){
            scope.loading_pricing=false;
          },0);
			   },
        error:function(data){
          $timeout(function(){
            scope.loading_pricing_error=true;
            scope.loading_pricing=false;
          },0);

        }
      });
		}
	};
}]);
app.directive('popOnHover', [function () {
	return {
		restrict: 'A',
		link: function (scope, el, attrs) {

            var timer;
			el.bind('mouseenter',function(){
				timer = setTimeout(function(){
                    if(attrs.popOnHover=="presentation") {
                        angular.element('.pophover').fadeIn();
                    }
                    else{
                        angular.element('.pophover-movie').fadeIn();
                    }
                },500);
			});
			el.bind('mouseleave',function(){
				if(attrs.popOnHover=="presentation") {
                    angular.element('.pophover').fadeOut();
                }
                else {
                    angular.element('.pophover-movie').fadeOut();
                }
                clearTimeout(timer);
			});
		}
	};
}]);
app.directive('resetCss', [function () {
	return {
		restrict: 'A',
		link: function (scope, el, iAttrs) {

			el.parent().parent().removeClass('wider');
			el.parent().parent().addClass('reset-css');
		}
	};
}]);

app.directive('onEnter',function() {
  var linkFn = function(scope,element,attrs) {
    element.bind("keypress", function(event) {
      if(event.which === 13) {
          scope.callback();
//        });
        event.preventDefault();
      }
    });
  };

  return {
    link:linkFn,
      scope: {
          callback: '='
      }
  };
});

app.directive('onLoadFade',function() {

  var linkFn = function(scope,element,attrs) {

      if(!scope.s.userData.thumb.length) {
        element.hide();
        element.bind("load", function (event) {
            element.fadeIn();
        });
    }
  };

  return {
    link:linkFn
  };
});
app.directive('showTooltipOnPremium',['$timeout',function($timeout) {

  var linkFn = function(scope,element,attrs) {
       var timer;

       element.bind("mouseenter",function(){
          timer=$timeout(function(){
            var tooltip = element.find('.tooltip').fadeIn();
            if (tooltip.size() > 0) {

              if (typeof(publish_source) === 'undefined') {
                  publish_source = 'publish_flow';
              }
              var promo_id = publish_source,
                  promo_name = 'Upgrade',
                  element_name = element.data('element-name'),
                  promo_creative = element.data('promo-creative');

              if(EC){
                  EC.addPromo({id: promo_id, name: promo_name, creative: promo_creative});
                  EC.setAction('detail');
                  trackEventByParams("Internal Promotions", "detail", element_name);
              }
            }
          },450);
       });
       element.bind("mouseleave",function(){
           $timeout.cancel(timer);
           $timeout(function(){element.find('.tooltip').fadeOut();},400);
       });


  };

  return {
    link:linkFn
  };
}]);
app.directive('closeTooltip',['$timeout',function($timeout) {
  var linkFn = function(scope,element,attrs) {
       element.bind("click",function(){
           element.parent().fadeOut();
       });
  };

  return {
    link:linkFn
  };
}]);
