angular.module('publishModalApp')
    .controller('UpgradeController', UpgradeController);

UpgradeController.$inject = ['$scope', '$location'];
function UpgradeController($scope, $location) {
    $scope.loading_pricing = false;
    $scope.loading_pricing_error = false;

    var contains = function (string, word) {
        return (string.indexOf(word) !== -1);
    };

    var sendGoogleEnhancedEcommerse = function () {
        var report = ["Upgraded",
            mainService.userData.plan.mode,
            "from",
            publish_source
        ].join(" ");

        if (EC && ga) {
            EC.addPromo(report);
            EC.setAction("promo_click");
            trackEventByParams("Internal Promotions", "click", report);
        }

    }
    sendGoogleEnhancedEcommerse();

    angular.extend($scope.$parent.modal, {
        backEnabled: true,
        nextEnabled: false,
        closeEnabled: false,
        stepHeadline: "Upgrade",
        next: "Details",
        back: function () {
            $location.path($scope.$parent.modal.before);
        },
        next: function () {
            $location.path($scope.$parent.routes.step_quality);
        }
    });
    $scope.step = "upgrade";
};
