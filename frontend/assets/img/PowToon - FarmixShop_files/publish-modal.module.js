angular.module('publishModalApp',
    [
        'ui.bootstrap',
        'ngRoute',
        'ngTagsInput',
        'ngSanitize',
        'rebirthComponents'
    ]);