(function () {
    'use strict';

    angular.module('publishModalApp')
        .controller('MainCtrl', MainCtrl);

    MainCtrl.$inject = ['$scope','$rootScope', '$location', 'mainService', '$timeout', 'Steps', 'Publish'];
    function MainCtrl($scope, $rootScope, $location, s, $timeout, Steps, Publish) {
        $location.path('');

        // Saves the function that we get from the $watch function.
        // Stops the watch upon invoking it.
        var stopWatchingValidation;

        $scope.init = function (powtoon, mode, category, publishTo, name, isRepublished, description, tags, powtoonCategory, aspectRatio) {
            Publish.init(powtoon, mode, category, publishTo, name, isRepublished, description, tags, powtoonCategory, aspectRatio);
            s.init(publishTo);
            s.vendor = publishTo;

            $scope.routeComplete = false;
            $scope.s = s;
            $scope.exportOptions = Publish.options;
            $scope.user = Publish.userData;
            $scope.exportAccounts = [];
            $scope.uploadVendor = Publish.options.uploadTo;
            $scope.loaded = true;
            $scope.vendor = publishTo;
            $scope.currentStep = 0;
            $scope.route = Steps.getRoute(publishTo);
            $scope.category = Publish.category;
            $scope.noWatermark = !Publish.userData.watermark;
            $scope.watermark = Publish.options.watermark;
            stopWatchingValidation = watchValidation();
            $location.path($scope.route[$scope.currentStep].path);
            $scope.modal.hideSteps = false;

            evaluateLabels();
            $(document).trigger('publish-modal-open');
        };

        $scope.validate = function () {
            $scope.validated = true;
        };


        $(document).keyup(function (e) {
            if (e.keyCode == 27) {
                $scope.closePublishFlow();
            }
        });

        $scope.closePublishFlow = function (source) {

            if (stopWatchingValidation instanceof Function) {
                stopWatchingValidation();
            }

            $('#publish-modal')
                .modal('hide');

            $location.path('');
            $scope.validated = true;

            // Broadcast closing event.
            $scope.$broadcast('close');
            $(document).trigger('publish-modal-close');

            if (source === 'default') {
                // now do nothing
            } else if (source === 'upgrade') {
                $('#publish_targets_modal').modal('show');
            } else {
                if (window.frameElement !== null && window.frameElement.id === 'publishIframe') {
                    window.parent.closePublishIframe();
                }
            }
        };

        $scope.closePublishFlowOrExitIframe = function () {
            $scope.closePublishFlow();
        };

        $rootScope.$on('$locationChangeSuccess', function() {
            var returnIndex = false;

            if ($scope.route) {
                $scope.route.forEach(function (node, index) {
                    if (node.path === $location.path()) {
                        returnIndex = index;
                    }
                });

                if (returnIndex !== false) {
                    $scope.currentStep = returnIndex;
                    evaluateLabels();
                }
            }
        });
        $scope.goToPricing = function (type, source) {
          if (typeof EC !== "undefined" && source && publish_source) {
            EC.addPromo({id: 'Publish_flow', name: 'Upgrade', creative: source, position: 'publish_modal'});
            EC.setAction("promo_click");
            trackEventByParams("upgrade_to_" + source, "upgrade", 'publish_flow_' + publish_source);
          }

          if (type === 'pro') {
            $location.path('/Publish/Loading');
            paymentPopups.showPricing();
            $scope.closePublishFlow('upgrade');
            
          } else {
            $location.path('/purchase/business_monthly');
            paymentPopups.showBizPlan();
            $scope.closePublishFlow('upgrade');
          }


            return true;
        };

        $scope.getDefaultPublishHeadline = function (textSuffix) {
            var modeText = (s.mode === 'Movie') ? 'Movie' : 'Slideshow';
            return 'Publish your PowToon as a ' + modeText + textSuffix;
        };

        $scope.modal = {
            nextLabel: "Next",
            backLabel: "Back",
            stepHeadline: "",
            nextEnabled: true,
            backEnabled: true,
            closeEnabled: true,
            hideSteps: false,
            next: function () {
                genericNext();
            },
            back: function () {
                genericBack();
            }
        };

        // Calls the step's validation function
        function genericNext() {
            $scope.validated = undefined;
            $timeout($scope.validate);
        }

        function genericBack() {
            if ($scope.currentStep - 1 >= 0) {
                $scope.currentStep--;
                evaluateLabels();
                $location.path($scope.route[$scope.currentStep].path);
            }
        }

        function firstStepBack() {
            $scope.closePublishFlowOrExitIframe();
        }


        // The mechanism of the publish flow steps chaning works like this -
        // Its listening to $scope.validated changes. its validated === true,
        // It goes to next step.
        function watchValidation() {
            // Watches the validation
            return $scope.$watch('validated', function (validated, oldVal) {
                // If there is no change
                if (validated === oldVal || validated === undefined) {
                    return null;
                    // If its validated.
                } else if (validated) {
                    gotoNextStep();
                }
            });
        }

        function gotoNextStep() {
            // If its the last step
            if ($scope.currentStep === $scope.route.length - 1) {
                $scope.export();
            } else {
                $scope.currentStep++;

                evaluateLabels();
                // Change the location to the next one.
                $location.path($scope.route[$scope.currentStep].path);
            }
        }

        function evaluateLabels() {
            // Disable back if its first step.
            if ($scope.currentStep === 0) {
                $scope.modal.back = firstStepBack;
                $scope.modal.backLabel = 'Cancel';
                $scope.modal.nextLabel = 'Next';
                $scope.modal.backEnabled = true;
                $scope.modal.nextEnabled = true;
            }
            // If its the last step
            else if ($scope.currentStep === $scope.route.length - 1) {
                $scope.modal.nextLabel = getExportLabel();
                $scope.modal.backLabel = 'Back';
                $scope.modal.back = genericBack;
                // If its every other step.
            } else {
                $scope.modal.back = genericBack;
                $scope.modal.next = genericNext;
                $scope.modal.nextLabel = 'Next';
                $scope.modal.backLabel = 'Back';
            }
        }

        function getExportLabel() {
            var exportLabel = 'Export PowToon';

            if (Publish.category === 'upload') {
                exportLabel = 'Upload PowToon';
            } else if (Publish.category === 'download') {
                exportLabel = 'Download PowToon';
            } else if (Publish.category === 'share') {
                exportLabel = 'Share PowToon';
            }

            return exportLabel;
        }

        $scope.export = function () {

            $scope.modal.nextEnabled = false;
            $scope.modal.backEnabled = false;

            Publish.publish().then(function () {
                var queryParams = Publish.mode ? '?mode=' + Publish.mode : '';
                queryParams += Publish.category === 'share' ? '&share=' + Publish.publishTo : '';

            var player_page_url = "/online-presentation/" + Publish.powtoon + queryParams;
            // in case publish flow is iframe close parent modal
            if (window.frameElement !== null && window.frameElement.id === 'publishIframe') {
                window.parent.publishRedirectSuccess(player_page_url);
            } else {
                window.location.href = player_page_url;
            }


            }).fail(function(){
                $scope.closePublishFlow();

            });

            $location.path('/Publish/Loading');
        }
    }
})();
