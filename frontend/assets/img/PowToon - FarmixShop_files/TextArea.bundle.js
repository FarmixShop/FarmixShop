var TextArea =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(1);
	
	var _react2 = _interopRequireDefault(_react);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var ReactCSSTransitionGroup = _react2.default.addons.CSSTransitionGroup;
	
	var TextArea = function (_React$Component) {
	    _inherits(TextArea, _React$Component);
	
	    function TextArea(props) {
	        _classCallCheck(this, TextArea);
	
	        var _this = _possibleConstructorReturn(this, (TextArea.__proto__ || Object.getPrototypeOf(TextArea)).call(this, props));
	
	        _this.state = {
	            value: "",
	            show_textarea: false,
	            textarea_content: true,
	            show_topbar: true
	        };
	
	        _this.handleEditClick = _this.handleEditClick.bind(_this);
	        _this.handleSave = _this.handleSave.bind(_this);
	        _this.handleCancel = _this.handleCancel.bind(_this);
	        _this.handleInitialValue = _this.handleInitialValue.bind(_this);
	        return _this;
	    }
	
	    _createClass(TextArea, [{
	        key: "render",
	        value: function render() {
	            return _react2.default.createElement(
	                "div",
	                { ref: "textarea_component", className: "react_textarea" },
	                this.state.show_topbar ? _react2.default.createElement(TopBar, { placeholder: this.props.placeholder, value: this.state.value, editPlaceholder: this.props.editPlaceholder,
	                    onClick: this.handleEditClick }) : null,
	                this.state.show_textarea ? _react2.default.createElement(TextAreaModal, {
	                    onSave: this.handleSave,
	                    onCancel: this.handleCancel,
	                    value: this.state.value,
	                    placeholder: this.props.textAreaPlaceholder || this.props.placeholder
	                }) : null
	            );
	        }
	    }, {
	        key: "handleEditClick",
	        value: function handleEditClick() {
	            this.setState({
	                show_textarea: true,
	                show_topbar: true
	            });
	        }
	    }, {
	        key: "handleSave",
	        value: function handleSave(newText) {
	            if (newText.split('\n').length < 9) {
	                this.setState({
	                    value: newText
	                });
	                if (this.props.onChangeCallback instanceof Function) {
	                    this.props.onChangeCallback.call({
	                        text: newText
	                    });
	                }
	            }
	
	            this.setState({
	                show_textarea: false,
	                show_topbar: true
	            });
	        }
	    }, {
	        key: "handleCancel",
	        value: function handleCancel() {
	            this.setState({
	                show_textarea: false,
	                show_topbar: true
	            });
	        }
	    }, {
	        key: "handleInitialValue",
	        value: function handleInitialValue() {
	            if (this.props.initialValue) {
	                this.setState({
	                    value: this.props.initialValue,
	                    show_textarea: false,
	                    show_topbar: true
	                });
	            }
	
	            if (this.props.onChangeCallback) {
	                this.props.onChangeCallback.call({
	                    text: this.props.initialValue
	                });
	            }
	        }
	    }, {
	        key: "componentDidMount",
	        value: function componentDidMount() {
	            this.handleInitialValue();
	        }
	    }, {
	        key: "componentWillUnmount",
	        value: function componentWillUnmount() {}
	    }]);
	
	    return TextArea;
	}(_react2.default.Component);
	
	var TopBar = _react2.default.createClass({
	    displayName: "TopBar",
	
	    render: function render() {
	        return _react2.default.createElement(
	            "div",
	            { className: "topbar" },
	            this.props.value ? null : _react2.default.createElement(TopBarLeft, { placeholder: this.props.placeholder,
	                onClick: this.props.onClick }),
	            this.props.value ? _react2.default.createElement(TopBarText, { value: this.props.value, placeholder: this.props.editPlaceholder }) : null,
	            this.props.value ? _react2.default.createElement(TopBarRight, { onClick: this.props.onClick }) : null
	        );
	    }
	});
	
	var TopBarLeft = _react2.default.createClass({
	    displayName: "TopBarLeft",
	
	    render: function render() {
	        return _react2.default.createElement(
	            "div",
	            { className: "left_link" },
	            _react2.default.createElement(
	                "button",
	                {
	                    onClick: this.props.onClick,
	                    className: "desc"
	                },
	                _react2.default.createElement(
	                    "h2",
	                    null,
	                    this.props.placeholder ? this.props.placeholder : 'Placeholder',
	                    " "
	                )
	            )
	        );
	    }
	});
	
	var TopBarText = _react2.default.createClass({
	    displayName: "TopBarText",
	
	    render: function render() {
	        return _react2.default.createElement(
	            "div",
	            null,
	            _react2.default.createElement(
	                "h2",
	                null,
	                this.props.placeholder
	            ),
	            _react2.default.createElement(
	                "span",
	                { className: "toptext", title: this.props.value },
	                this.props.value.split(',').join(', ')
	            )
	        );
	    }
	});
	
	var TopBarRight = _react2.default.createClass({
	    displayName: "TopBarRight",
	
	    render: function render() {
	        return _react2.default.createElement(
	            "div",
	            { onClick: this.props.onClick, className: "rightlink" },
	            _react2.default.createElement("button", { className: "rightedit text-4 pticon-pencil" })
	        );
	    }
	});
	
	var TextAreaModal = function (_React$Component2) {
	    _inherits(TextAreaModal, _React$Component2);
	
	    function TextAreaModal(props) {
	        _classCallCheck(this, TextAreaModal);
	
	        var _this2 = _possibleConstructorReturn(this, (TextAreaModal.__proto__ || Object.getPrototypeOf(TextAreaModal)).call(this, props));
	
	        _this2.state = {
	            text: _this2.props.value,
	            btnSave: {
	                caption: "save",
	                isDisabled: true,
	                onClick: _this2.onSave.bind(_this2)
	            },
	            btnCancel: {
	                caption: "cancel",
	                onClick: _this2.props.onCancel
	            }
	        };
	
	        _this2.onTextChange = _this2.onTextChange.bind(_this2);
	        return _this2;
	    }
	
	    _createClass(TextAreaModal, [{
	        key: "onTextChange",
	        value: function onTextChange(event) {
	            this.setState({
	                text: event.target.value,
	                btnSave: $.extend(this.state.btnSave, { isDisabled: false }) // enable save button
	            });
	        }
	    }, {
	        key: "onSave",
	        value: function onSave() {
	            this.props.onSave(this.state.text);
	        }
	    }, {
	        key: "render",
	        value: function render() {
	            return _react2.default.createElement(
	                ReactCSSTransitionGroup,
	                { transitionEnterTimeout: 500, transitionAppear: true, transitionName: "example" },
	                _react2.default.createElement(
	                    "div",
	                    { className: "textarea-modal" },
	                    _react2.default.createElement(TextAreaBody, { value: this.state.text, onChange: this.onTextChange, placeholder: this.props.placeholder }),
	                    _react2.default.createElement(TextAreaFooter, { btnSave: this.state.btnSave, btnCancel: this.state.btnCancel })
	                )
	            );
	        }
	    }]);
	
	    return TextAreaModal;
	}(_react2.default.Component);
	
	function selectText(event) {
	    var target = event.target;
	    setTimeout(function () {
	        target.select();
	    });
	}
	
	var TextAreaBody = _react2.default.createClass({
	    displayName: "TextAreaBody",
	
	    render: function render() {
	        return _react2.default.createElement(
	            "div",
	            { className: "textAreaBody" },
	            _react2.default.createElement("textarea", {
	                maxLength: 750, rows: "4", cols: "50", value: this.props.value, onChange: this.props.onChange,
	                placeholder: this.props.placeholder, autoFocus: true, onFocus: selectText })
	        );
	    }
	});
	
	var TextAreaFooter = _react2.default.createClass({
	    displayName: "TextAreaFooter",
	
	    render: function render() {
	        return _react2.default.createElement(
	            "div",
	            { className: "text-area-footer textArea-bottom" },
	            _react2.default.createElement(
	                "div",
	                { className: "align-left" },
	                _react2.default.createElement(
	                    "button",
	                    { onClick: this.props.btnCancel.onClick },
	                    _react2.default.createElement(
	                        "h2",
	                        null,
	                        this.props.btnCancel.caption
	                    )
	                )
	            ),
	            _react2.default.createElement(
	                "div",
	                { className: "align-right" },
	                _react2.default.createElement(
	                    "button",
	                    { onClick: this.props.btnSave.onClick, disabled: this.props.btnSave.isDisabled },
	                    _react2.default.createElement(
	                        "h2",
	                        null,
	                        this.props.btnSave.caption
	                    )
	                )
	            )
	        );
	    }
	});
	
	module.exports = TextArea;

/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = React;

/***/ }
/******/ ]);
//# sourceMappingURL=TextArea.bundle.js.map