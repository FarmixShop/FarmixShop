angular.module('publishModalApp')
    .controller('VimeoController', VimeoController);

VimeoController.$inject = ['$scope','mainService', 'Publish'];
function VimeoController($scope, s, Publish) {
    $scope.vimeoAccount = Publish.options.vimeoAccount;
    $scope.vimeoUrl = Publish.userData.vimeo_url;
    $scope.vimeoAccounts = Publish.userData.vimeo_accounts;
    $scope.loginError = false;

    $(document).unbind('loginComplete');
    $(document).unbind('retrieve');
    $(document).unbind('loginError');

    $scope.$parent.validate = function () {
        if (Publish.options.vimeoAccount && Publish.userData.vimeo_accounts.length) {
            $scope.$parent.validated = true;
        }
        else {
            $scope.$parent.validated = false;
        }
    };

    $scope.addAccount = function () {
        $(window)[0].open(Publish.userData.vimeo_url, 'Login', 'width=900,height=700');
        $(document).bind('loginComplete', function () {
            s.getVimeoAccountDetails();
        });

        $(document).bind('retrieve.vimeo', function () {
            $scope.vimeoAccount = Publish.options.vimeoAccount;
            $scope.vimeoAccounts = Publish.userData.vimeo_accounts;
        });

        $(document).bind('loginError', function (e, data) {
            $scope.$parent.validated = false;
            $scope.loginError = true;
        });
    };

    $scope.$on('close', function () {
        cleanErrors();
    });

    function cleanErrors() {

    }

    $(document).bind('retrieve.vimeo', function () {
        $scope.vimeoAccount = Publish.options.vimeoAccount;
        $scope.vimeoAccounts = Publish.userData.vimeo_accounts;
    });

    $scope.removeAccount = function (account) {
        $('.preloader').fadeIn();
        s.deleteVimeoAccount(account).then(function () {
            if ($scope.vimeoAccounts.length && account.id === $scope.vimeoAccount) {
                $scope.vimeoAccount = $scope.vimeoAccounts[0].id;
                Publish.options.vimeoAccount = $scope.vimeoAccounts[0].id;
            } else if ($scope.vimeoAccounts.length === 0) {
                $scope.vimeoAccount = false;
                Publish.options.vimeoAccount = false;
            }
        });
    };

    $scope.setVimeoAccount = function (id) {
        $scope.vimeoAccount = id;
        s.setVimeoAccount(id);
    }
}

