/**
 * Created by orenlevi on 1/13/16.
 */

(function () {
    'use strict';

    /* Definitions */

    var hideClass = 'hide-player';

    /**
     * Defines a controller for toggle button
     * @param {string} element - jQuery selector for area containing the following classes:
     *          'js-checkbox'       - the actual checkbox element which has 'checked' attribute to indicate mode
     *          'js-toggle-area'    - area that holds the entire UI of the toggle button
     *          'js-loader-area'    - area that holds the entire UI of the loader
     * @constructor - each object exposes these methods:
     *      setMode(mode)       -   set the toggle mode
     *      getMode()           -   returns a string ('movie'/'presentation') indicating the mode of the toggle button
     *      onChange(callback)  -   invokes callback(newMode) when the mode of the toggle changes as a result
     *                              of a user action
     *      disable()           -   displays loader instead of the toggle button
     *      enable()            -   displays toggle button and hides loader
     */
    function ModeToggleCtrl(element) {
        /* Private members */

        var self = this,
            $element = $(element),
            $btnElement = $element.find('.js-checkbox'),
            $btnAreaElement = $element.find('.js-toggle-area'),
            $loaderAreaElement = $element.find('.js-loader-area');

        /* Public members */

        self.setMode = function (mode) {
            $btnElement.prop('checked', mode === 'movie');
        };

        self.getMode = function () {
            return ($btnElement.is(':checked') ? 'movie' : 'presentation');
        };

        self.onChange = function(callback) {
            if (typeof callback === 'function') {
                $btnElement.on('click', function() { // invoke onChange with getmode data when button is clicked
                    callback(self.getMode());
                    var toggleMode = 'movie';
                    if (self.getMode() === 'presentation')
                        toggleMode = 'slideshow';
                    trackEventByParams('player', 'click', toggleMode);
                });
            }
        };

        self.disable = function () {
            $btnAreaElement.addClass(hideClass);
            $loaderAreaElement.removeClass(hideClass);
        };

        self.enable = function () {
            $loaderAreaElement.addClass(hideClass);
            $btnAreaElement.removeClass(hideClass);
        };
    }

    /**
     * Defines a controller for presenting the relevant export buttons
     * @param {object} elementMap - object containing the following fields:
     *          parentButtonArea -jQuery selector for area containing both movie and presentation buttons
     *          movieButtonArea - jQuery selector for area containing movie export buttons
     *          presentationButtonArea - jQuery selector for area containing presentation export buttons
     *          updateModeGroup - jQuery selector for a group of buttons that their DOM 'data-publish-mode' attribute
     *              will be updated according to selected mode
     * @constructor - each object exposes these methods:
     *          init(mode)      - shows the export button area and sets it's mode
     *          setMode(mode)   - presents only the export buttons that matches the given mode
     */
    function ExportButtonsCtrl(elementMap) {
        /* Private members */

        var self = this,
            $parentButtonArea = $(elementMap.parentButtonArea),
            $movieButtonArea = $(elementMap.movieButtonArea),
            $presentationButtonArea = $(elementMap.presentationButtonArea),
            $updateModeGroup = $(elementMap.parentButtonArea).find(elementMap.updateModeGroup);

        function setUpdateGroupMode(mode) {
            $updateModeGroup.data('publish-mode', mode);
        }

        function setMovie() {
            $presentationButtonArea.addClass(hideClass);
            $movieButtonArea.removeClass(hideClass);
            setUpdateGroupMode('movie');
        }

        function setPresentation() {
            $movieButtonArea.addClass(hideClass);
            $presentationButtonArea.removeClass(hideClass);
            setUpdateGroupMode('presentation');
        }

        /* Public members */

        self.init = function (mode) {
            $parentButtonArea.removeClass(hideClass);
            self.setMode(mode);
        };

        self.setMode = function (mode) {
            if (mode === 'movie') {
                setMovie();
            } else {
                setPresentation();
            }
        };
    }

    /**
     * Defines a controller for dealing with a single url query parameter
     * @param {string} paramName - query parameter name
     * @constructor - each object exposes these methods:
     *      get()       - returns the value {string} of the query param or null if such query param does not exists
     *      set(mode)   - sets the value of the query param
     *      remove()    - removes the parameter from the url query
     */
    function UrlQueryCtrl(paramName) {
        /* Private members */

        var self = this,
            re = new RegExp('[\?|&]' + paramName + '=([^&]*)', 'i');

        function getClearQueryString() {
            // returns the url query that contains everything BUT the parameter
            return window.location.search
                .replace(re, '')        // removes '?param=value' or '&param=value' from string
                .replace(/^&/, '?');    // replaces '&' in the string start with '?' if exists
        }

        function changeQueryString(newQueryString) {
            if (window.history && typeof window.history.pushState === 'function') {
                // changes the url without reload
                window.history.pushState('', '', newQueryString);
            } else {
                // changes the url and reload
                window.location.search = newQueryString;
            }
        }

        /* Public members */

        self.get = function () { // gets the parameter if present in the query string, null otherwise
            try {
                var value = re.exec(window.location.search)[1];
                return decodeURIComponent(value);
            } catch (e) {
                return null; // if the desired pattern isn't present we just return null
            }
        };

        self.set = function (value) {
            var clearQueryString = getClearQueryString(),
                prefix = clearQueryString ? '&' : '?';

            changeQueryString(clearQueryString + prefix + paramName + '=' + encodeURIComponent(value));
        };

        self.remove = function () {
            return changeQueryString( getClearQueryString() );
        };
    }

    /**
     * Defines a controller for dealing with powtoon flash player on player page
     * @param {string} element  - jQuery selector for area containing the player
     * @constructor - each object exposes these methods:
     *      setMode(mode)               -   sets the player mode {string} to 'movie' or 'presentation'
     *      init()                      -   initializes player
     *      onLoadComplete(callback)    -   invokes callback() when player load is completed
     *                                      (or immediately if player is already loaded)
     */
    function PowtoonFlashPlayerCtrl(element) {
        /* Private members */

        var self = this,
            $element = $(element);

        /* Public members */

        self.setMode = function (mode) {
            try {
                // 'PowtoonApp' is the object that the flash player exposes
                PowtoonApp.setMode(mode);
            } catch (err) {
                Raven.captureException(
                    new Error('Unable to set mode for player, make sure it has finished loading.\n' + err.toString())
                );
            }
        };

        self.init = function () {
            $element.removeClass(hideClass);
        };

        self.onLoadComplete = function (callback) {
            if (typeof callback === 'function') {
                if (window.appLoaded) {
                    callback();
                } else {
                    $(document).one('flash-player-loaded', callback);
                }
            }
        };
    }


    /**
     * Defines a controller for switching between external video player and powtoon presentation player
     * @param {object} elementMap - object containing the following fields:
     *      externalPlayerArea  - jQuery selector for area containing external player
     *      powtoonPlayerArea   - jQuery selector for area containing powtoon player
     *      playerEmbedArea     - jQuery selector for area containing both players
     * @constructor - each object exposes these methods:
     *      setMode(mode)               - sets the player mode {string} to 'movie' or 'presentation'
     *      init(mode)                  - initializes the player to initial mode
     *      onLoadComplete(callback)    - invokes callback() when player load is completed
     *                                    (or immediately if player is already loaded)
     */
    function PowtoonHtml5PlayerCtrl(element, mode) {
        /* Private members */

        var self = this,
            $element = $(element),
            initialMode = mode,
            playerApi;


        self.onPlayerReady = function () {
          $('#player-embed-area .preloader').hide();
          if (mode === 'presentation') {
            playerApi.setSlideshowMode();
          }
        };

        /* Public members */

        self.init = function () {
            $element.removeClass(hideClass);
             var iframe = document.getElementById('playerIframe');
             playerApi = new PlayerApi(iframe, self.onPlayerReady);

            // remove preloader failover in case Iframe doesn't fire event
            setTimeout(function(){
                $('#player-embed-area .preloader').hide();
            }, 10000);

        };



        self.setMode = function (mode) {
          playerApi.pause();
          playerApi.jumpToStart();

          if (mode === 'presentation') {
            playerApi.setSlideshowMode();
          }  else {
            playerApi.setMovieMode();
          }
        };

        self.onLoadComplete = function (callback) {
            if (typeof callback === 'function') {
                callback();
            }
        };
    }

    /**
     * Defines a controller for switching between external video player and powtoon presentation player
     * @param {object} elementMap - object containing the following fields:
     *      externalPlayerArea  - jQuery selector for area containing external player
     *      powtoonPlayerArea   - jQuery selector for area containing powtoon player
     *      playerEmbedArea     - jQuery selector for area containing both players
     * @constructor - each object exposes these methods:
     *      setMode(mode)               - sets the player mode {string} to 'movie' or 'presentation'
     *      init(mode)                  - initializes the player to initial mode
     *      onLoadComplete(callback)    - invokes callback() when player load is completed
     *                                    (or immediately if player is already loaded)
     */
    function ExternalVideoPlayerCtrl(elementMap, isHtml5Player) {
        /* Private members */

        var self = this,
            $externalPlayerArea = $(elementMap.externalPlayerArea),
            $powtoonPlayerArea = $(elementMap.powtoonPlayerArea),
            $playerEmbedArea = $(elementMap.playerEmbedArea);

        function showExternalPlayer() {
            $externalPlayerArea.removeClass(hideClass);
            $powtoonPlayerArea.addClass(hideClass);
            $playerEmbedArea.addClass('external-video-player');
        }

        function showPowtoonPlayer() {
            $powtoonPlayerArea.removeClass(hideClass);
            $externalPlayerArea.addClass(hideClass);
            $playerEmbedArea.removeClass('external-video-player');
            if (window.youtubePlayer && window.youtubePlayer.pauseVideo) {
                window.youtubePlayer.pauseVideo(); // pausing youtube player
            }

            if (isHtml5Player === true) {
                setTimeout(function () {
                    $('#player-embed-area .preloader').hide();
                }, 3000); //since the html player loads on page load no need for long timeout here
            }
        }

        /* Public members */

        self.setMode = function (mode) {
            if (mode === 'movie') {
                showExternalPlayer();
            } else {
                showPowtoonPlayer();
            }
        };

        self.init = self.setMode;

        self.onLoadComplete = function (callback) {
            if (typeof callback === 'function') {
                callback();
            }
        };
    }

    /**
     * Returns matching player controller according to provided isExternalPlayer param
     * @param {bool} isExternalPlayer
     * @returns ExternalVideoPlayerCtrl object when (isExternalPlayer=true) or PowtoonFlashPlayerCtrl otherwise
     */
    function playerCtrlFactory(isExternalPlayer, isHtml5Player, initialDisplayMode) {
        var playerCtrl;

        if (isExternalPlayer) {
            playerCtrl = new ExternalVideoPlayerCtrl({
                externalPlayerArea: '#wistia-player',
                powtoonPlayerArea: '#powtoon-player',
                playerEmbedArea: '#player-embed-area'
            }, isHtml5Player);
        } else if (isHtml5Player) {
            playerCtrl = new PowtoonHtml5PlayerCtrl('#powtoon-player', initialDisplayMode);
        } else {
            playerCtrl = new PowtoonFlashPlayerCtrl('#powtoon-player');
        }

        return playerCtrl;
    }

    /**
     * main code
     * @param {bool}    isExternalPlayer    - true/false
     * @param {string}  initialDisplayMode  - 'movie'/'presentation'
     * @param {object}  exportObj           - (optional) object to export functions to
     */
    function init(isExternalPlayer, isHtml5Player, initialDisplayMode, exportObj) {
        var modeToggleCtrl = new ModeToggleCtrl('#mode-toggle'),
            playerCtrl = playerCtrlFactory(isExternalPlayer, isHtml5Player, initialDisplayMode),
            exportButtonCtrl = new ExportButtonsCtrl({
                parentButtonArea: '#export-button-area',
                movieButtonArea: '#movie-button-area',
                presentationButtonArea: '#presentation-button-area',
                updateModeGroup: '.js-update-my-mode'
            }),
            modeUrlQueryCtrl = new UrlQueryCtrl('mode'),
            shareUrlQueryCtrl = new UrlQueryCtrl('share'),
            shareVendor = shareUrlQueryCtrl.get(),
            displayMode = modeUrlQueryCtrl.get() || initialDisplayMode;

        exportButtonCtrl.init(displayMode);
        modeToggleCtrl.setMode(displayMode);

        playerCtrl.onLoadComplete(modeToggleCtrl.enable);
        playerCtrl.init(displayMode);

        modeToggleCtrl.onChange(function(newMode) {
            modeUrlQueryCtrl.set(newMode);
            playerCtrl.setMode(newMode);
            exportButtonCtrl.setMode(newMode);
            window.displayMode = newMode;
        });

        if (modeUrlQueryCtrl.get() === null) {
            modeUrlQueryCtrl.set(initialDisplayMode);
        }

        shareUrlQueryCtrl.remove();

        /**
         * Opens a share dialog when shareVendor is a valid share option
         * @param {string} shareVendor - sharing vendor (facebook, linkedin ...)
         */
        function openShareOption(shareVendor) {
            if (shareVendor) {
                var selector = '#share-' + shareVendor;
                if ($(selector).length === 0) {
                    Raven.captureException(new Error('No such sharing option: ' + selector));
                } else {
                    $(selector).trigger('click');
                }
            }
        }

        /**
         * Deals with openning share dialog when page loads
         */
        setTimeout(function () {
            openShareOption(shareVendor);
        });

        /**
         * Exports
         * All members in this hash will be exposed as members in the exportObj
         */
        $.extend(exportObj, {
            getPlayerMode: modeUrlQueryCtrl.get
        });
    }

    /* Executed code */
    $(function () {
        init(window.isExternalPlayer, window.isHtml5Player, window.displayMode.toLowerCase(), window);
    });

})();
