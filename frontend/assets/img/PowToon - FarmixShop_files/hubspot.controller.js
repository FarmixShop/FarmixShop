angular.module('publishModalApp')
    .controller('HubspotController', HubspotController);

HubspotController.$inject = ['$scope','mainService', 'Publish'];
function HubspotController($scope, s, Publish) {
    $scope.hubspotAccount = Publish.options.hubspotAccount;
    $scope.hubspotUrl = Publish.userData.hubspot_url;
    $scope.hubspotAccounts = Publish.userData.hubspot_accounts;
    $scope.loginError = false;

    console.log('Publish.options.hubspotAccount', Publish.options.hubspotAccount)

    $(document).unbind('loginComplete');
    $(document).unbind('retrieve');
    $(document).unbind('loginError');

    $scope.$parent.validate = function () {
        if (Publish.options.hubspotAccount && Publish.userData.hubspot_accounts.length) {
            $scope.$parent.validated = true;
        }
        else {
            $scope.$parent.validated = false;
        }
    };

    $scope.addAccount = function () {
        $(window)[0].open(Publish.userData.hubspot_url, 'Login', 'width=900,height=700');
        $(document).bind('loginComplete', function () {
            s.getHubspotAccountDetails();
        });

        $(document).bind('retrieve.hubspot', function () {
            $scope.hubspotAccount = Publish.options.hubspotAccount;
            $scope.hubspotAccounts = Publish.userData.hubspot_accounts;
        });

        $(document).bind('loginError', function (e, data) {
            $scope.$parent.validated = false;
            $scope.loginError = true;
        });
    };

    $scope.$on('close', function () {
        cleanErrors();
    });

    function cleanErrors() {

    }

    $(document).bind('retrieve.hubspot', function () {
        $scope.hubspotAccount = Publish.options.hubspotAccount;
        $scope.hubspotAccounts = Publish.userData.hubspot_accounts;
    });

    $scope.removeAccount = function (account) {
        $('.preloader').fadeIn();
        s.deleteHubspotAccount(account).then(function () {
            if ($scope.hubspotAccounts.length && account.id === $scope.hubspotAccount) {
                $scope.hubspotAccount = $scope.hubspotAccounts[0].id;
                Publish.options.hubspotAccount = $scope.hubspotAccounts[0].id;
            } else if ($scope.hubspotAccounts.length === 0) {
                $scope.hubspotAccount = false;
                Publish.options.hubspotAccount = false;
            }
        });
    };

    $scope.setHubspotAccount = function (id) {
        $scope.hubspotAccount = id;
        s.setHubspotAccount(id);
    }
}

