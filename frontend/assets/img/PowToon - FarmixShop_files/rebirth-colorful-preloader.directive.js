(function () {
    'use strict';

    angular.module('rebirthComponents', []);
    angular.module('rebirthComponents').directive('rebirthColorfulPreloader', rebirthColorfulPreloader);

    function rebirthColorfulPreloader() {
        var directive = {
            restrict: 'E',
            link: link,
            template: '<canvas class="rebirth-colorful-preloader-container" width="250" height="200">  </canvas>',
        };

        function link(scope, element, attrs) {
            var canvas = element.find('.rebirth-colorful-preloader-container')[0];
            var stage, exportRoot;

            init();

            function init() {
                exportRoot = new lib.preloaderHtmlfile();

                stage = new createjs.Stage(canvas);
                stage.addChild(exportRoot);
                stage.update();

                createjs.Ticker.setFPS(lib.properties.fps);
                createjs.Ticker.addEventListener("tick", stage);
            }
        }

        return directive;
    }
})();