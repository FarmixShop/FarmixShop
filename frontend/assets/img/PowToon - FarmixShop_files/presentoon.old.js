var Presentoon = function () {
    function getById(id) {
        return $.ajax({
            url: '/api/v1/presentoon/' + id,
            type: 'GET'
        });
    }

    function publishAllowed(id) {
        return $.ajax({
            url: '/api/v1/presentoon/' + id + '/publish_allowed'
        })
    }

    return {
        getById: getById,
        publishAllowed: publishAllowed
    }
}();
