var app = angular.module('publishModalApp').config(function ($routeProvider, $locationProvider) {
    $routeProvider.when('/Publish/YoutubeAccount', {
            templateUrl: 'youtube/youtube.html',
            controller: 'YoutubeController'
        })
        .when('/Publish/FacebookAccount', {
            templateUrl: 'facebook/facebook.html',
            controller: 'FacebookController'
        })
        .when('/Publish/Quality', {
            templateUrl: 'quality/quality.html',
            controller: 'QualityController'
        })
        .when('/Publish/Loading', {
            templateUrl: 'loading/loading.html',
            controller: 'LoadingController'
        })
        .when('/Publish/WistiaAccount', {
            templateUrl: 'wistia/wistia.html',
            controller: 'WistiaController'
        })
        .when('/Publish/VimeoAccount', {
            templateUrl: 'vimeo/vimeo.html',
            controller: 'VimeoController'
        })
        .when('/Publish/HubspotAccount', {
            templateUrl: 'hubspot/hubspot.html',
            controller: 'HubspotController'
        })
        .when('/Publish/Details', {
            templateUrl: 'details/details.html',
            controller: 'DetailsController'
        })
        .when('/Publish/SlideshareAccount', {
            templateUrl: 'slideshare/slideshare.html',
            controller: 'SlideshareController'
        })
        .when('/Loading', {
            templateUrl: 'loading/loading.html',
            controller: 'LoadingController'
        })
        .when('/Error', {
            templateUrl: 'error/error.html',
            controller: 'ErrorController'
        })
        .when('/', {
            template: '',
            controller: 'defaultController'
        })
        .otherwise({redirectTo: '/'});
    $locationProvider.html5Mode = true;
}).config(["$interpolateProvider", "$httpProvider", "$sceDelegateProvider",
    function ($interpolateProvider, $httpProvider, $sceDelegateProvider) {
        $sceDelegateProvider.resourceUrlWhitelist([
            // Allow same origin resource loads.
            'self',
            // Allow loading from outer templates domain.
            'https://*.powtoon.com/**',
            'https://*.powtoon.co/**',
            'http://powtoon-*-static.s3.amazonaws.com/**',
            'https://powtoon-*-static.s3.amazonaws.com/**',
            'http://*.global.ssl.fastly.net/**',
            'https://*.global.ssl.fastly.net/**',
            'https://d1nmwcjvqo9f2e.cloudfront.net/**',
            'https://d2ci509untjdsc.cloudfront.net/**'
        ]);
        $interpolateProvider.startSymbol('{$');
        $interpolateProvider.endSymbol('$}');
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    }]);

