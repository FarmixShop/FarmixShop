function openPublishTargets(hashid, name, mode, aspectRatio) {
  if(!aspectRatio){
      aspectRatio = "None";
  }

  ReactDOM.render (
      React.createElement(
          PublishTargets,
          {
            hashid: hashid,
            name: name,
            mode: mode,
            aspectRatio: aspectRatio
          }
      ),
      document.getElementById('publish-targets-layout')
  );

  $('#publish_targets_modal').modal('show');
}


(function () {

  // click on dashboard publish circle
  $('.pt-list').on('click', '.js-publish-button', function (e) {
    e.stopPropagation();

    var hashid = $(this).closest('.js-presentoon-row').attr('pid');
    var name = $(this).closest('.js-presentoon-row').attr('name');
    var mode = $(this).closest('.js-presentoon-row').data('mode');
    var aspectRatio = $(this).closest('.js-presentoon-row').attr('aspectRatio') || 'None';

    openPublishTargets(hashid, name, mode, aspectRatio);
  });

  // click export from player page
  $('.player-extra').on('click', '.js-publish-button', function (e) {
    e.stopPropagation();

    openPublishTargets(window.hash_id, window.presentoon_name, window.displayMode, window.aspect_ratio || 'none');
    trackEventByParams("export_options", "click", "Player_page-owner");
  });
})();
