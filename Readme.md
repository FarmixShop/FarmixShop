# FARMIXSHOP

El proyecto consiste en una aplicación web construida con **PHP Angular y MySQL**



## Presentación del proyecto


### Frontend

El Frontend esta construido con **HTML5, CSS3, JavaScript, JQuery**

Se utilizan los frameworks:

```
 Bootstrap 4
 Angular 1.4
```

### Backend

El Backend esta construido con **PHP**

Se utilizan las versiones:

```
 PHP 5
```

## Modulos del proyecto

## Home
Modulo de inicio que cargara cuando el usuario inicie la aplicación, esta construido integramente con información sobre el contenido de la web

![HOME](https://lh3.googleusercontent.com/-_k1amrmB1yI/WgtfQRokgzI/AAAAAAAAAAk/PNAKxQIWJVY_5k5wRRJf7Yk8PE6MMstFQCL0BGAs/w530-d-h286-n-rw/Captura%2Bde%2Bpantalla%2Bde%2B2017-11-14%2B22-23-49.png)



### Shop

>El modulo contiene una visualizacion en lista de todos los usuarios que existen en la base de datos, contiene ademas un mapa donde se puede previsualizar la posicion de ese usuario.
Dentro del list destacamos el sistema de busqueda, donde cualquier usuario puede buscar productos, el sistema filtrara, mostrando los usuarios que tienen ese producto disponible.
Entre las funcionalidades de la aplicación esta, cuando cualquier usuario hace click en Ver Mas de cualquier usuario mostrara un 'details' donde se muestra un detalle de ese usuario, los productos que tienen disponibles y su ubicación.

#### Funcionalidades
* List
* Details
* Search
* Geolocalizacion

List
![HOME](https://lh3.googleusercontent.com/-XjNFJywZWTQ/WgtlhvdGoII/AAAAAAAAABM/yE-kffSMufEA3TLbzT5o2VzKr3Nexi2_gCL0BGAs/w530-d-h213-n-rw/Captura%2Bde%2Bpantalla%2Bde%2B2017-11-14%2B22-51-24.png)

Details
![HOME](https://lh3.googleusercontent.com/-qAmuYMuDt-g/WgtmGMc6BrI/AAAAAAAAABk/bgYL5YBgeUQRktXTKLoYe6zGYknamfqBQCL0BGAs/w530-d-h195-n-rw/Captura%2Bde%2Bpantalla%2Bde%2B2017-11-14%2B22-54-13.png)


## LogIn

>Este modulo permite el signin/login a través de las redes sociales de Twitter y Facebook y login a partir de un correo y contraseña.

A parte de esta funcionalidad encontramos: un recover password que nos servira si el usuario no se acuerda de la cntraseña y un pofile, donde podra ver su perfil y cambiarlo si hay algun error.

#### Funcionalidades
* SignIn/Login Twitter-Facebook
* SignUp con datos de el usuaraio
* LogIn con datos de el usuario
* Recover Password
* Profile

El profile ha sido modificado dadas las directivas decididas por el grupo, una de las directivas añadidas es la de que el usuario puede guardar cualquier grupos de datos independientemente.
> * El primer grupo de datos seria el de los datos personales de el usuario
* En el segundo grupo entra la modificacion de la contraseña
* El tercer grupo consiste en la modificación de el avatar
* Finalmente el ultimo grupo es un mapa donde el usuario puede modificar su posicion y guardarla

## Contact

>El modulo de contact es un modulo normal en la mayoria de aplicaciones web que se encuentran en Internet, su funcion principal es la de enviar un correo de soporte al equipo tecnico, para que solucionen su problema.

#### Funcionalidades
* Enviar el correo a el soporte tectnico


### Modulos extra utilizados

```
** Modulo de LocalStorage ** se utiliza para guardar la informacion que el sistema necesita para funcionar correctamente
** Modulo de Cookies ** su funcion principal es la de guardar informacion de la sesion de el usuario
```

## APIs Utilizadas

Dentro de las APIs que se utilizan estan: 
* Api de Twitter, se utiliza para el login de el usuario
* Api de Facebook, se utiliza para el login de el usuario
* Api de Google Maps, geolocalizacion
* Api de NgMap, tambien utilizada para la golocalozacion, pero en el profile.

## Authors

* **Sergio Martínez**
* **Óscar Martínez**